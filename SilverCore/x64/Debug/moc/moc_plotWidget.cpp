/****************************************************************************
** Meta object code from reading C++ file 'plotWidget.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.12.3)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../../inc/plotWidget.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'plotWidget.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.12.3. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_PlotWidget_t {
    QByteArrayData data[29];
    char stringdata0[349];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_PlotWidget_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_PlotWidget_t qt_meta_stringdata_PlotWidget = {
    {
QT_MOC_LITERAL(0, 0, 10), // "PlotWidget"
QT_MOC_LITERAL(1, 11, 15), // "operationRecord"
QT_MOC_LITERAL(2, 27, 0), // ""
QT_MOC_LITERAL(3, 28, 8), // "fa::icon"
QT_MOC_LITERAL(4, 37, 4), // "icon"
QT_MOC_LITERAL(5, 42, 6), // "record"
QT_MOC_LITERAL(6, 49, 11), // "clearCurves"
QT_MOC_LITERAL(7, 61, 16), // "xAxisChangeRange"
QT_MOC_LITERAL(8, 78, 13), // "tracerClicked"
QT_MOC_LITERAL(9, 92, 7), // "checked"
QT_MOC_LITERAL(10, 100, 16), // "selectShowChange"
QT_MOC_LITERAL(11, 117, 10), // "showSwitch"
QT_MOC_LITERAL(12, 128, 18), // "xAxisCangeCallBack"
QT_MOC_LITERAL(13, 147, 8), // "QCPRange"
QT_MOC_LITERAL(14, 156, 5), // "range"
QT_MOC_LITERAL(15, 162, 10), // "selectNone"
QT_MOC_LITERAL(16, 173, 9), // "selectAll"
QT_MOC_LITERAL(17, 183, 17), // "curveColorChanged"
QT_MOC_LITERAL(18, 201, 5), // "color"
QT_MOC_LITERAL(19, 207, 16), // "curveNameChanged"
QT_MOC_LITERAL(20, 224, 4), // "name"
QT_MOC_LITERAL(21, 229, 18), // "traceTargetChanged"
QT_MOC_LITERAL(22, 248, 12), // "curvesSearch"
QT_MOC_LITERAL(23, 261, 4), // "text"
QT_MOC_LITERAL(24, 266, 12), // "numberUpdate"
QT_MOC_LITERAL(25, 279, 22), // "curvesScrollBarChanged"
QT_MOC_LITERAL(26, 302, 5), // "value"
QT_MOC_LITERAL(27, 308, 21), // "curvesScrollBarAction"
QT_MOC_LITERAL(28, 330, 18) // "curvesXAxisChanged"

    },
    "PlotWidget\0operationRecord\0\0fa::icon\0"
    "icon\0record\0clearCurves\0xAxisChangeRange\0"
    "tracerClicked\0checked\0selectShowChange\0"
    "showSwitch\0xAxisCangeCallBack\0QCPRange\0"
    "range\0selectNone\0selectAll\0curveColorChanged\0"
    "color\0curveNameChanged\0name\0"
    "traceTargetChanged\0curvesSearch\0text\0"
    "numberUpdate\0curvesScrollBarChanged\0"
    "value\0curvesScrollBarAction\0"
    "curvesXAxisChanged"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_PlotWidget[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
      17,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       2,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    2,   99,    2, 0x06 /* Public */,
       6,    0,  104,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
       7,    0,  105,    2, 0x08 /* Private */,
       8,    1,  106,    2, 0x08 /* Private */,
      10,    1,  109,    2, 0x08 /* Private */,
      11,    0,  112,    2, 0x08 /* Private */,
      12,    1,  113,    2, 0x08 /* Private */,
      15,    0,  116,    2, 0x08 /* Private */,
      16,    0,  117,    2, 0x08 /* Private */,
      17,    1,  118,    2, 0x08 /* Private */,
      19,    1,  121,    2, 0x08 /* Private */,
      21,    0,  124,    2, 0x08 /* Private */,
      22,    1,  125,    2, 0x08 /* Private */,
      24,    1,  128,    2, 0x08 /* Private */,
      25,    1,  131,    2, 0x08 /* Private */,
      27,    1,  134,    2, 0x08 /* Private */,
      28,    1,  137,    2, 0x08 /* Private */,

 // signals: parameters
    QMetaType::Void, 0x80000000 | 3, QMetaType::QString,    4,    5,
    QMetaType::Void,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void, QMetaType::Bool,    9,
    QMetaType::Void, QMetaType::Bool,    9,
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 13,   14,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::QColor,   18,
    QMetaType::Void, QMetaType::QString,   20,
    QMetaType::Void,
    QMetaType::Void, QMetaType::QString,   23,
    QMetaType::Void, QMetaType::Bool,    9,
    QMetaType::Void, QMetaType::Int,   26,
    QMetaType::Void, QMetaType::Int,   26,
    QMetaType::Void, 0x80000000 | 13,   14,

       0        // eod
};

void PlotWidget::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<PlotWidget *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->operationRecord((*reinterpret_cast< fa::icon(*)>(_a[1])),(*reinterpret_cast< QString(*)>(_a[2]))); break;
        case 1: _t->clearCurves(); break;
        case 2: _t->xAxisChangeRange(); break;
        case 3: _t->tracerClicked((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 4: _t->selectShowChange((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 5: _t->showSwitch(); break;
        case 6: _t->xAxisCangeCallBack((*reinterpret_cast< QCPRange(*)>(_a[1]))); break;
        case 7: _t->selectNone(); break;
        case 8: _t->selectAll(); break;
        case 9: _t->curveColorChanged((*reinterpret_cast< QColor(*)>(_a[1]))); break;
        case 10: _t->curveNameChanged((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 11: _t->traceTargetChanged(); break;
        case 12: _t->curvesSearch((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 13: _t->numberUpdate((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 14: _t->curvesScrollBarChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 15: _t->curvesScrollBarAction((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 16: _t->curvesXAxisChanged((*reinterpret_cast< QCPRange(*)>(_a[1]))); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        {
            using _t = void (PlotWidget::*)(fa::icon , QString );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&PlotWidget::operationRecord)) {
                *result = 0;
                return;
            }
        }
        {
            using _t = void (PlotWidget::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&PlotWidget::clearCurves)) {
                *result = 1;
                return;
            }
        }
    }
}

QT_INIT_METAOBJECT const QMetaObject PlotWidget::staticMetaObject = { {
    &QWidget::staticMetaObject,
    qt_meta_stringdata_PlotWidget.data,
    qt_meta_data_PlotWidget,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *PlotWidget::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *PlotWidget::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_PlotWidget.stringdata0))
        return static_cast<void*>(this);
    return QWidget::qt_metacast(_clname);
}

int PlotWidget::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 17)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 17;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 17)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 17;
    }
    return _id;
}

// SIGNAL 0
void PlotWidget::operationRecord(fa::icon _t1, QString _t2)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void PlotWidget::clearCurves()
{
    QMetaObject::activate(this, &staticMetaObject, 1, nullptr);
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
