/****************************************************************************
** Meta object code from reading C++ file 'SilverCore.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.12.3)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../../inc/SilverCore.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'SilverCore.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.12.3. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_SilverCore_t {
    QByteArrayData data[23];
    char stringdata0[270];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_SilverCore_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_SilverCore_t qt_meta_stringdata_SilverCore = {
    {
QT_MOC_LITERAL(0, 0, 10), // "SilverCore"
QT_MOC_LITERAL(1, 11, 23), // "setConnectControlEnable"
QT_MOC_LITERAL(2, 35, 0), // ""
QT_MOC_LITERAL(3, 36, 6), // "enable"
QT_MOC_LITERAL(4, 43, 15), // "operationRecord"
QT_MOC_LITERAL(5, 59, 8), // "fa::icon"
QT_MOC_LITERAL(6, 68, 4), // "icon"
QT_MOC_LITERAL(7, 73, 6), // "record"
QT_MOC_LITERAL(8, 80, 10), // "pageSwitch"
QT_MOC_LITERAL(9, 91, 17), // "clearCacheClicked"
QT_MOC_LITERAL(10, 109, 18), // "refreshPortClicked"
QT_MOC_LITERAL(11, 128, 17), // "connectComClicked"
QT_MOC_LITERAL(12, 146, 18), // "selectSpeedChanged"
QT_MOC_LITERAL(13, 165, 4), // "rate"
QT_MOC_LITERAL(14, 170, 17), // "selectPortChanged"
QT_MOC_LITERAL(15, 188, 4), // "port"
QT_MOC_LITERAL(16, 193, 17), // "handleSerialError"
QT_MOC_LITERAL(17, 211, 4), // "tips"
QT_MOC_LITERAL(18, 216, 12), // "displayTxNum"
QT_MOC_LITERAL(19, 229, 8), // "uint64_t"
QT_MOC_LITERAL(20, 238, 6), // "length"
QT_MOC_LITERAL(21, 245, 12), // "displayRxNum"
QT_MOC_LITERAL(22, 258, 11) // "connectPort"

    },
    "SilverCore\0setConnectControlEnable\0\0"
    "enable\0operationRecord\0fa::icon\0icon\0"
    "record\0pageSwitch\0clearCacheClicked\0"
    "refreshPortClicked\0connectComClicked\0"
    "selectSpeedChanged\0rate\0selectPortChanged\0"
    "port\0handleSerialError\0tips\0displayTxNum\0"
    "uint64_t\0length\0displayRxNum\0connectPort"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_SilverCore[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
      12,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       1,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    1,   74,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
       4,    2,   77,    2, 0x08 /* Private */,
       8,    0,   82,    2, 0x08 /* Private */,
       9,    0,   83,    2, 0x08 /* Private */,
      10,    0,   84,    2, 0x08 /* Private */,
      11,    0,   85,    2, 0x08 /* Private */,
      12,    1,   86,    2, 0x08 /* Private */,
      14,    1,   89,    2, 0x08 /* Private */,
      16,    1,   92,    2, 0x08 /* Private */,
      18,    1,   95,    2, 0x08 /* Private */,
      21,    1,   98,    2, 0x08 /* Private */,
      22,    1,  101,    2, 0x08 /* Private */,

 // signals: parameters
    QMetaType::Void, QMetaType::Bool,    3,

 // slots: parameters
    QMetaType::Void, 0x80000000 | 5, QMetaType::QString,    6,    7,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::QString,   13,
    QMetaType::Void, QMetaType::QString,   15,
    QMetaType::Void, QMetaType::QString,   17,
    QMetaType::Void, 0x80000000 | 19,   20,
    QMetaType::Void, 0x80000000 | 19,   20,
    QMetaType::Void, QMetaType::Bool,    3,

       0        // eod
};

void SilverCore::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<SilverCore *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->setConnectControlEnable((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 1: _t->operationRecord((*reinterpret_cast< fa::icon(*)>(_a[1])),(*reinterpret_cast< QString(*)>(_a[2]))); break;
        case 2: _t->pageSwitch(); break;
        case 3: _t->clearCacheClicked(); break;
        case 4: _t->refreshPortClicked(); break;
        case 5: _t->connectComClicked(); break;
        case 6: _t->selectSpeedChanged((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 7: _t->selectPortChanged((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 8: _t->handleSerialError((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 9: _t->displayTxNum((*reinterpret_cast< uint64_t(*)>(_a[1]))); break;
        case 10: _t->displayRxNum((*reinterpret_cast< uint64_t(*)>(_a[1]))); break;
        case 11: _t->connectPort((*reinterpret_cast< bool(*)>(_a[1]))); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        {
            using _t = void (SilverCore::*)(bool );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&SilverCore::setConnectControlEnable)) {
                *result = 0;
                return;
            }
        }
    }
}

QT_INIT_METAOBJECT const QMetaObject SilverCore::staticMetaObject = { {
    &QDialog::staticMetaObject,
    qt_meta_stringdata_SilverCore.data,
    qt_meta_data_SilverCore,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *SilverCore::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *SilverCore::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_SilverCore.stringdata0))
        return static_cast<void*>(this);
    return QDialog::qt_metacast(_clname);
}

int SilverCore::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QDialog::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 12)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 12;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 12)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 12;
    }
    return _id;
}

// SIGNAL 0
void SilverCore::setConnectControlEnable(bool _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
