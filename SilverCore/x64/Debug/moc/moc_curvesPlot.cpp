/****************************************************************************
** Meta object code from reading C++ file 'curvesPlot.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.12.3)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../../inc/curvesPlot.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'curvesPlot.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.12.3. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_CurvesPlot_t {
    QByteArrayData data[20];
    char stringdata0[251];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_CurvesPlot_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_CurvesPlot_t qt_meta_stringdata_CurvesPlot = {
    {
QT_MOC_LITERAL(0, 0, 10), // "CurvesPlot"
QT_MOC_LITERAL(1, 11, 18), // "noniusValueChanged"
QT_MOC_LITERAL(2, 30, 0), // ""
QT_MOC_LITERAL(3, 31, 9), // "_curveIdx"
QT_MOC_LITERAL(4, 41, 3), // "val"
QT_MOC_LITERAL(5, 45, 17), // "autoRangeAdjusted"
QT_MOC_LITERAL(6, 63, 8), // "QCPRange"
QT_MOC_LITERAL(7, 72, 5), // "range"
QT_MOC_LITERAL(8, 78, 14), // "storeAxisScope"
QT_MOC_LITERAL(9, 93, 15), // "resumeAxisScope"
QT_MOC_LITERAL(10, 109, 12), // "showAllGraph"
QT_MOC_LITERAL(11, 122, 12), // "clearAllData"
QT_MOC_LITERAL(12, 135, 27), // "when_selectionChangedByUser"
QT_MOC_LITERAL(13, 163, 22), // "when_legendDoubleClick"
QT_MOC_LITERAL(14, 186, 10), // "QCPLegend*"
QT_MOC_LITERAL(15, 197, 6), // "legend"
QT_MOC_LITERAL(16, 204, 22), // "QCPAbstractLegendItem*"
QT_MOC_LITERAL(17, 227, 4), // "item"
QT_MOC_LITERAL(18, 232, 12), // "QMouseEvent*"
QT_MOC_LITERAL(19, 245, 5) // "event"

    },
    "CurvesPlot\0noniusValueChanged\0\0_curveIdx\0"
    "val\0autoRangeAdjusted\0QCPRange\0range\0"
    "storeAxisScope\0resumeAxisScope\0"
    "showAllGraph\0clearAllData\0"
    "when_selectionChangedByUser\0"
    "when_legendDoubleClick\0QCPLegend*\0"
    "legend\0QCPAbstractLegendItem*\0item\0"
    "QMouseEvent*\0event"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_CurvesPlot[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
       8,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       2,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    2,   54,    2, 0x06 /* Public */,
       5,    1,   59,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
       8,    0,   62,    2, 0x0a /* Public */,
       9,    0,   63,    2, 0x0a /* Public */,
      10,    0,   64,    2, 0x0a /* Public */,
      11,    0,   65,    2, 0x0a /* Public */,
      12,    0,   66,    2, 0x08 /* Private */,
      13,    3,   67,    2, 0x08 /* Private */,

 // signals: parameters
    QMetaType::Void, QMetaType::Int, QMetaType::QString,    3,    4,
    QMetaType::Void, 0x80000000 | 6,    7,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 14, 0x80000000 | 16, 0x80000000 | 18,   15,   17,   19,

       0        // eod
};

void CurvesPlot::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<CurvesPlot *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->noniusValueChanged((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< QString(*)>(_a[2]))); break;
        case 1: _t->autoRangeAdjusted((*reinterpret_cast< QCPRange(*)>(_a[1]))); break;
        case 2: _t->storeAxisScope(); break;
        case 3: _t->resumeAxisScope(); break;
        case 4: _t->showAllGraph(); break;
        case 5: _t->clearAllData(); break;
        case 6: _t->when_selectionChangedByUser(); break;
        case 7: _t->when_legendDoubleClick((*reinterpret_cast< QCPLegend*(*)>(_a[1])),(*reinterpret_cast< QCPAbstractLegendItem*(*)>(_a[2])),(*reinterpret_cast< QMouseEvent*(*)>(_a[3]))); break;
        default: ;
        }
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        switch (_id) {
        default: *reinterpret_cast<int*>(_a[0]) = -1; break;
        case 7:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 1:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QCPAbstractLegendItem* >(); break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QCPLegend* >(); break;
            }
            break;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        {
            using _t = void (CurvesPlot::*)(int , QString );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&CurvesPlot::noniusValueChanged)) {
                *result = 0;
                return;
            }
        }
        {
            using _t = void (CurvesPlot::*)(QCPRange );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&CurvesPlot::autoRangeAdjusted)) {
                *result = 1;
                return;
            }
        }
    }
}

QT_INIT_METAOBJECT const QMetaObject CurvesPlot::staticMetaObject = { {
    &QCustomPlot::staticMetaObject,
    qt_meta_stringdata_CurvesPlot.data,
    qt_meta_data_CurvesPlot,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *CurvesPlot::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *CurvesPlot::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_CurvesPlot.stringdata0))
        return static_cast<void*>(this);
    return QCustomPlot::qt_metacast(_clname);
}

int CurvesPlot::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QCustomPlot::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 8)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 8;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 8)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 8;
    }
    return _id;
}

// SIGNAL 0
void CurvesPlot::noniusValueChanged(int _t1, QString _t2)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void CurvesPlot::autoRangeAdjusted(QCPRange _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
