/********************************************************************************
** Form generated from reading UI file 'SilverCoreUi.ui'
**
** Created by: Qt User Interface Compiler version 5.12.3
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_SILVERCOREUI_H
#define UI_SILVERCOREUI_H

#include <QtCore/QVariant>
#include <QtGui/QIcon>
#include <QtWidgets/QApplication>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QDialog>
#include <QtWidgets/QFrame>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QStackedWidget>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>
#include "navbutton.h"

QT_BEGIN_NAMESPACE

class Ui_SilverCoreUi
{
public:
    QGridLayout *gridLayout;
    QHBoxLayout *horizontalLayout;
    QVBoxLayout *verticalLayout;
    NavButton *dataDisplayBtn;
    NavButton *curvesDisplayBtn;
    NavButton *configBtn;
    NavButton *calibrateBtn;
    NavButton *firmwareBtn;
    NavButton *expandBtn;
    QStackedWidget *stackedWidget;
    QFrame *line;
    QWidget *connectWidget;
    QHBoxLayout *horizontalLayout_4;
    QLabel *tipsIconLabel;
    QLabel *tipsTextLabel;
    QSpacerItem *horizontalSpacer_4;
    QGroupBox *rxGroupBox;
    QHBoxLayout *horizontalLayout_2;
    QLabel *RxLabel;
    QLabel *RxNumLabel;
    QGroupBox *txGroupBox;
    QHBoxLayout *horizontalLayout_3;
    QLabel *TxLabel;
    QLabel *TxNumLabel;
    QSpacerItem *horizontalSpacer_5;
    QSpacerItem *horizontalSpacer_6;
    QPushButton *clearBtn;
    QFrame *line_2;
    QGroupBox *speedGroupBox;
    QHBoxLayout *horizontalLayout_5;
    QLabel *speedLabel;
    QComboBox *rateSelectBox;
    QGroupBox *portGroupBox;
    QHBoxLayout *horizontalLayout_6;
    QLabel *portLabel;
    QComboBox *portSelectBox;
    QSpacerItem *horizontalSpacer;
    QPushButton *refreshBtn;
    QSpacerItem *horizontalSpacer_2;
    QPushButton *connectBtn;

    void setupUi(QDialog *SilverCoreUi)
    {
        if (SilverCoreUi->objectName().isEmpty())
            SilverCoreUi->setObjectName(QString::fromUtf8("SilverCoreUi"));
        SilverCoreUi->setWindowModality(Qt::NonModal);
        SilverCoreUi->resize(1280, 800);
        SilverCoreUi->setMinimumSize(QSize(1280, 800));
        QFont font;
        font.setFamily(QString::fromUtf8("\345\276\256\350\275\257\351\233\205\351\273\221"));
        SilverCoreUi->setFont(font);
        SilverCoreUi->setContextMenuPolicy(Qt::NoContextMenu);
        QIcon icon;
        icon.addFile(QString::fromUtf8(":/SilverCore/Resources/speed_plane_128px_1179597_easyicon.net.ico"), QSize(), QIcon::Normal, QIcon::Off);
        SilverCoreUi->setWindowIcon(icon);
        SilverCoreUi->setStyleSheet(QString::fromUtf8(""));
        SilverCoreUi->setModal(false);
        gridLayout = new QGridLayout(SilverCoreUi);
        gridLayout->setSpacing(0);
        gridLayout->setContentsMargins(11, 11, 11, 11);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        gridLayout->setContentsMargins(0, 0, 0, 0);
        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setSpacing(0);
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        verticalLayout = new QVBoxLayout();
        verticalLayout->setSpacing(0);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        dataDisplayBtn = new NavButton(SilverCoreUi);
        dataDisplayBtn->setObjectName(QString::fromUtf8("dataDisplayBtn"));
        QSizePolicy sizePolicy(QSizePolicy::Minimum, QSizePolicy::Minimum);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(dataDisplayBtn->sizePolicy().hasHeightForWidth());
        dataDisplayBtn->setSizePolicy(sizePolicy);
        dataDisplayBtn->setMinimumSize(QSize(100, 0));
        dataDisplayBtn->setMaximumSize(QSize(100, 16777215));
        dataDisplayBtn->setStyleSheet(QString::fromUtf8(""));
        dataDisplayBtn->setChecked(true);
        dataDisplayBtn->setAutoExclusive(true);
        dataDisplayBtn->setAutoDefault(false);
        dataDisplayBtn->setFlat(false);
        dataDisplayBtn->setPaddingLeft(25);
        dataDisplayBtn->setPaddingBottom(30);
        dataDisplayBtn->setTextAlign(NavButton::TextAlign_Bottom);
        dataDisplayBtn->setShowTriangle(true);
        dataDisplayBtn->setShowIcon(true);
        dataDisplayBtn->setIconSpace(35);
        dataDisplayBtn->setIconSize(QSize(30, 30));
        dataDisplayBtn->setLineSpace(0);
        dataDisplayBtn->setNormalBgColor(QColor(183, 201, 207));

        verticalLayout->addWidget(dataDisplayBtn);

        curvesDisplayBtn = new NavButton(SilverCoreUi);
        curvesDisplayBtn->setObjectName(QString::fromUtf8("curvesDisplayBtn"));
        sizePolicy.setHeightForWidth(curvesDisplayBtn->sizePolicy().hasHeightForWidth());
        curvesDisplayBtn->setSizePolicy(sizePolicy);
        curvesDisplayBtn->setMinimumSize(QSize(100, 0));
        curvesDisplayBtn->setMaximumSize(QSize(100, 16777215));
        curvesDisplayBtn->setChecked(false);
        curvesDisplayBtn->setAutoExclusive(true);
        curvesDisplayBtn->setAutoDefault(false);
        curvesDisplayBtn->setPaddingLeft(25);
        curvesDisplayBtn->setPaddingBottom(30);
        curvesDisplayBtn->setTextAlign(NavButton::TextAlign_Bottom);
        curvesDisplayBtn->setShowTriangle(true);
        curvesDisplayBtn->setShowIcon(true);
        curvesDisplayBtn->setIconSpace(35);
        curvesDisplayBtn->setIconSize(QSize(30, 30));
        curvesDisplayBtn->setLinePosition(NavButton::LinePosition_Left);
        curvesDisplayBtn->setNormalBgColor(QColor(183, 201, 207));

        verticalLayout->addWidget(curvesDisplayBtn);

        configBtn = new NavButton(SilverCoreUi);
        configBtn->setObjectName(QString::fromUtf8("configBtn"));
        sizePolicy.setHeightForWidth(configBtn->sizePolicy().hasHeightForWidth());
        configBtn->setSizePolicy(sizePolicy);
        configBtn->setMinimumSize(QSize(100, 0));
        configBtn->setMaximumSize(QSize(100, 16777215));
        configBtn->setChecked(false);
        configBtn->setAutoExclusive(true);
        configBtn->setAutoDefault(false);
        configBtn->setPaddingLeft(25);
        configBtn->setPaddingBottom(30);
        configBtn->setTextAlign(NavButton::TextAlign_Bottom);
        configBtn->setShowTriangle(true);
        configBtn->setShowIcon(true);
        configBtn->setIconSpace(35);
        configBtn->setIconSize(QSize(30, 30));
        configBtn->setLinePosition(NavButton::LinePosition_Left);
        configBtn->setNormalBgColor(QColor(183, 201, 207));

        verticalLayout->addWidget(configBtn);

        calibrateBtn = new NavButton(SilverCoreUi);
        calibrateBtn->setObjectName(QString::fromUtf8("calibrateBtn"));
        sizePolicy.setHeightForWidth(calibrateBtn->sizePolicy().hasHeightForWidth());
        calibrateBtn->setSizePolicy(sizePolicy);
        calibrateBtn->setMinimumSize(QSize(100, 0));
        calibrateBtn->setMaximumSize(QSize(100, 16777215));
        calibrateBtn->setStyleSheet(QString::fromUtf8(""));
        calibrateBtn->setChecked(false);
        calibrateBtn->setAutoExclusive(true);
        calibrateBtn->setAutoDefault(false);
        calibrateBtn->setFlat(false);
        calibrateBtn->setPaddingLeft(25);
        calibrateBtn->setPaddingBottom(30);
        calibrateBtn->setTextAlign(NavButton::TextAlign_Bottom);
        calibrateBtn->setShowTriangle(true);
        calibrateBtn->setShowIcon(true);
        calibrateBtn->setIconSpace(35);
        calibrateBtn->setIconSize(QSize(30, 30));
        calibrateBtn->setLineSpace(0);
        calibrateBtn->setNormalBgColor(QColor(183, 201, 207));

        verticalLayout->addWidget(calibrateBtn);

        firmwareBtn = new NavButton(SilverCoreUi);
        firmwareBtn->setObjectName(QString::fromUtf8("firmwareBtn"));
        sizePolicy.setHeightForWidth(firmwareBtn->sizePolicy().hasHeightForWidth());
        firmwareBtn->setSizePolicy(sizePolicy);
        firmwareBtn->setMinimumSize(QSize(100, 0));
        firmwareBtn->setMaximumSize(QSize(100, 16777215));
        firmwareBtn->setStyleSheet(QString::fromUtf8(""));
        firmwareBtn->setChecked(false);
        firmwareBtn->setAutoExclusive(true);
        firmwareBtn->setAutoDefault(false);
        firmwareBtn->setFlat(false);
        firmwareBtn->setPaddingLeft(25);
        firmwareBtn->setPaddingBottom(30);
        firmwareBtn->setTextAlign(NavButton::TextAlign_Bottom);
        firmwareBtn->setShowTriangle(true);
        firmwareBtn->setShowIcon(true);
        firmwareBtn->setIconSpace(35);
        firmwareBtn->setIconSize(QSize(30, 30));
        firmwareBtn->setLineSpace(0);
        firmwareBtn->setNormalBgColor(QColor(183, 201, 207));

        verticalLayout->addWidget(firmwareBtn);

        expandBtn = new NavButton(SilverCoreUi);
        expandBtn->setObjectName(QString::fromUtf8("expandBtn"));
        sizePolicy.setHeightForWidth(expandBtn->sizePolicy().hasHeightForWidth());
        expandBtn->setSizePolicy(sizePolicy);
        expandBtn->setMinimumSize(QSize(100, 0));
        expandBtn->setMaximumSize(QSize(100, 16777215));
        expandBtn->setStyleSheet(QString::fromUtf8(""));
        expandBtn->setChecked(false);
        expandBtn->setAutoExclusive(true);
        expandBtn->setAutoDefault(false);
        expandBtn->setFlat(false);
        expandBtn->setPaddingLeft(25);
        expandBtn->setPaddingBottom(30);
        expandBtn->setTextAlign(NavButton::TextAlign_Bottom);
        expandBtn->setShowTriangle(true);
        expandBtn->setShowIcon(true);
        expandBtn->setIconSpace(35);
        expandBtn->setIconSize(QSize(30, 30));
        expandBtn->setLineSpace(0);
        expandBtn->setNormalBgColor(QColor(183, 201, 207));

        verticalLayout->addWidget(expandBtn);


        horizontalLayout->addLayout(verticalLayout);

        stackedWidget = new QStackedWidget(SilverCoreUi);
        stackedWidget->setObjectName(QString::fromUtf8("stackedWidget"));

        horizontalLayout->addWidget(stackedWidget);


        gridLayout->addLayout(horizontalLayout, 0, 0, 1, 1);

        line = new QFrame(SilverCoreUi);
        line->setObjectName(QString::fromUtf8("line"));
        line->setMinimumSize(QSize(0, 1));
        line->setMaximumSize(QSize(16777215, 1));
        line->setMidLineWidth(1);
        line->setFrameShape(QFrame::HLine);
        line->setFrameShadow(QFrame::Sunken);

        gridLayout->addWidget(line, 1, 0, 1, 1);

        connectWidget = new QWidget(SilverCoreUi);
        connectWidget->setObjectName(QString::fromUtf8("connectWidget"));
        connectWidget->setMinimumSize(QSize(0, 25));
        connectWidget->setMaximumSize(QSize(16777215, 25));
        horizontalLayout_4 = new QHBoxLayout(connectWidget);
        horizontalLayout_4->setSpacing(6);
        horizontalLayout_4->setContentsMargins(11, 11, 11, 11);
        horizontalLayout_4->setObjectName(QString::fromUtf8("horizontalLayout_4"));
        horizontalLayout_4->setContentsMargins(0, 0, 0, 0);
        tipsIconLabel = new QLabel(connectWidget);
        tipsIconLabel->setObjectName(QString::fromUtf8("tipsIconLabel"));
        tipsIconLabel->setMinimumSize(QSize(25, 25));
        tipsIconLabel->setMaximumSize(QSize(25, 25));

        horizontalLayout_4->addWidget(tipsIconLabel);

        tipsTextLabel = new QLabel(connectWidget);
        tipsTextLabel->setObjectName(QString::fromUtf8("tipsTextLabel"));
        tipsTextLabel->setMinimumSize(QSize(400, 0));
        tipsTextLabel->setMaximumSize(QSize(400, 16777215));
        QFont font1;
        font1.setPointSize(8);
        tipsTextLabel->setFont(font1);

        horizontalLayout_4->addWidget(tipsTextLabel);

        horizontalSpacer_4 = new QSpacerItem(664, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_4->addItem(horizontalSpacer_4);

        rxGroupBox = new QGroupBox(connectWidget);
        rxGroupBox->setObjectName(QString::fromUtf8("rxGroupBox"));
        rxGroupBox->setMinimumSize(QSize(90, 23));
        rxGroupBox->setMaximumSize(QSize(90, 23));
        horizontalLayout_2 = new QHBoxLayout(rxGroupBox);
        horizontalLayout_2->setSpacing(0);
        horizontalLayout_2->setContentsMargins(11, 11, 11, 11);
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        horizontalLayout_2->setContentsMargins(0, 0, 0, 0);
        RxLabel = new QLabel(rxGroupBox);
        RxLabel->setObjectName(QString::fromUtf8("RxLabel"));
        RxLabel->setMinimumSize(QSize(20, 20));
        RxLabel->setMaximumSize(QSize(20, 20));
        RxLabel->setSizeIncrement(QSize(30, 20));
        RxLabel->setFont(font1);

        horizontalLayout_2->addWidget(RxLabel);

        RxNumLabel = new QLabel(rxGroupBox);
        RxNumLabel->setObjectName(QString::fromUtf8("RxNumLabel"));
        RxNumLabel->setMinimumSize(QSize(60, 20));
        RxNumLabel->setMaximumSize(QSize(60, 20));
        RxNumLabel->setSizeIncrement(QSize(80, 20));
        RxNumLabel->setFont(font1);
        RxNumLabel->setAlignment(Qt::AlignCenter);

        horizontalLayout_2->addWidget(RxNumLabel);


        horizontalLayout_4->addWidget(rxGroupBox);

        txGroupBox = new QGroupBox(connectWidget);
        txGroupBox->setObjectName(QString::fromUtf8("txGroupBox"));
        txGroupBox->setMinimumSize(QSize(90, 23));
        txGroupBox->setMaximumSize(QSize(90, 23));
        horizontalLayout_3 = new QHBoxLayout(txGroupBox);
        horizontalLayout_3->setSpacing(0);
        horizontalLayout_3->setContentsMargins(11, 11, 11, 11);
        horizontalLayout_3->setObjectName(QString::fromUtf8("horizontalLayout_3"));
        horizontalLayout_3->setContentsMargins(0, 0, 0, 0);
        TxLabel = new QLabel(txGroupBox);
        TxLabel->setObjectName(QString::fromUtf8("TxLabel"));
        TxLabel->setMinimumSize(QSize(20, 20));
        TxLabel->setMaximumSize(QSize(20, 20));
        TxLabel->setSizeIncrement(QSize(30, 20));
        TxLabel->setFont(font1);

        horizontalLayout_3->addWidget(TxLabel);

        TxNumLabel = new QLabel(txGroupBox);
        TxNumLabel->setObjectName(QString::fromUtf8("TxNumLabel"));
        TxNumLabel->setMinimumSize(QSize(60, 20));
        TxNumLabel->setMaximumSize(QSize(60, 20));
        TxNumLabel->setSizeIncrement(QSize(80, 20));
        TxNumLabel->setFont(font1);
        TxNumLabel->setAlignment(Qt::AlignCenter);

        horizontalLayout_3->addWidget(TxNumLabel);


        horizontalLayout_4->addWidget(txGroupBox);

        horizontalSpacer_5 = new QSpacerItem(10, 20, QSizePolicy::Maximum, QSizePolicy::Minimum);

        horizontalLayout_4->addItem(horizontalSpacer_5);

        horizontalSpacer_6 = new QSpacerItem(18, 23, QSizePolicy::Maximum, QSizePolicy::Minimum);

        horizontalLayout_4->addItem(horizontalSpacer_6);

        clearBtn = new QPushButton(connectWidget);
        clearBtn->setObjectName(QString::fromUtf8("clearBtn"));
        clearBtn->setMinimumSize(QSize(92, 25));
        clearBtn->setMaximumSize(QSize(92, 25));
        clearBtn->setCursor(QCursor(Qt::PointingHandCursor));
        clearBtn->setStyleSheet(QString::fromUtf8(" QPushButton {\n"
"color: #94ffff;\n"
"border: 1px solid #888888;\n"
"font: 9pt \"\345\276\256\350\275\257\351\233\205\351\273\221\";\n"
"border-radius: 0px;\n"
"padding: 5px;\n"
"background: qradialgradient(cx: 0.3, cy: -0.4,\n"
"fx: 0.3, fy: -0.4,\n"
"radius: 0.57, stop: 0 #ffffff, stop: 1 #6cbcc8);\n"
"min-width: 80px;\n"
"}\n"
"\n"
"QPushButton:hover {\n"
"background: qradialgradient(cx: 0.3, cy: -0.4,\n"
"fx: 0.3, fy: -0.4,\n"
"radius: 0.57, stop: 0 #ffffff, stop: 1 #9bc3c8);\n"
"}\n"
"\n"
" QPushButton:pressed {\n"
"background: qradialgradient(cx: 0.4, cy: -0.1,\n"
"fx: 0.4, fy: -0.1,\n"
"radius: 0.57, stop: 0 #ffffff, stop: 1 #b1c6c8);\n"
"}\n"
"\n"
"QPushButton:disabled {\n"
"background: qradialgradient(cx: 0.4, cy: -0.1,\n"
"fx: 0.4, fy: -0.1,\n"
"radius: 0.57, stop: 0 #ffffff, stop: 1 #b1c6c8);\n"
"}"));

        horizontalLayout_4->addWidget(clearBtn);

        line_2 = new QFrame(connectWidget);
        line_2->setObjectName(QString::fromUtf8("line_2"));
        line_2->setMinimumSize(QSize(1, 0));
        line_2->setMaximumSize(QSize(1, 16777215));
        line_2->setMidLineWidth(1);
        line_2->setFrameShape(QFrame::VLine);
        line_2->setFrameShadow(QFrame::Sunken);

        horizontalLayout_4->addWidget(line_2);

        speedGroupBox = new QGroupBox(connectWidget);
        speedGroupBox->setObjectName(QString::fromUtf8("speedGroupBox"));
        speedGroupBox->setMinimumSize(QSize(0, 23));
        speedGroupBox->setMaximumSize(QSize(16777215, 23));
        horizontalLayout_5 = new QHBoxLayout(speedGroupBox);
        horizontalLayout_5->setSpacing(0);
        horizontalLayout_5->setContentsMargins(11, 11, 11, 11);
        horizontalLayout_5->setObjectName(QString::fromUtf8("horizontalLayout_5"));
        horizontalLayout_5->setContentsMargins(0, 0, 0, 0);
        speedLabel = new QLabel(speedGroupBox);
        speedLabel->setObjectName(QString::fromUtf8("speedLabel"));
        speedLabel->setMinimumSize(QSize(40, 20));
        speedLabel->setMaximumSize(QSize(40, 20));
        speedLabel->setSizeIncrement(QSize(30, 20));
        speedLabel->setFont(font1);

        horizontalLayout_5->addWidget(speedLabel);

        rateSelectBox = new QComboBox(speedGroupBox);
        rateSelectBox->addItem(QString());
        rateSelectBox->addItem(QString());
        rateSelectBox->addItem(QString());
        rateSelectBox->addItem(QString());
        rateSelectBox->addItem(QString());
        rateSelectBox->setObjectName(QString::fromUtf8("rateSelectBox"));
        rateSelectBox->setEnabled(true);
        rateSelectBox->setMinimumSize(QSize(80, 20));
        rateSelectBox->setMaximumSize(QSize(80, 20));
        rateSelectBox->setFont(font1);

        horizontalLayout_5->addWidget(rateSelectBox);


        horizontalLayout_4->addWidget(speedGroupBox);

        portGroupBox = new QGroupBox(connectWidget);
        portGroupBox->setObjectName(QString::fromUtf8("portGroupBox"));
        portGroupBox->setMinimumSize(QSize(0, 23));
        portGroupBox->setMaximumSize(QSize(16777215, 23));
        horizontalLayout_6 = new QHBoxLayout(portGroupBox);
        horizontalLayout_6->setSpacing(0);
        horizontalLayout_6->setContentsMargins(11, 11, 11, 11);
        horizontalLayout_6->setObjectName(QString::fromUtf8("horizontalLayout_6"));
        horizontalLayout_6->setContentsMargins(0, 0, 0, 0);
        portLabel = new QLabel(portGroupBox);
        portLabel->setObjectName(QString::fromUtf8("portLabel"));
        portLabel->setMinimumSize(QSize(30, 20));
        portLabel->setMaximumSize(QSize(30, 20));
        portLabel->setSizeIncrement(QSize(30, 20));
        portLabel->setFont(font1);

        horizontalLayout_6->addWidget(portLabel);

        portSelectBox = new QComboBox(portGroupBox);
        portSelectBox->setObjectName(QString::fromUtf8("portSelectBox"));
        portSelectBox->setMinimumSize(QSize(80, 20));
        portSelectBox->setMaximumSize(QSize(80, 20));
        portSelectBox->setFont(font1);

        horizontalLayout_6->addWidget(portSelectBox);


        horizontalLayout_4->addWidget(portGroupBox);

        horizontalSpacer = new QSpacerItem(20, 20, QSizePolicy::Maximum, QSizePolicy::Minimum);

        horizontalLayout_4->addItem(horizontalSpacer);

        refreshBtn = new QPushButton(connectWidget);
        refreshBtn->setObjectName(QString::fromUtf8("refreshBtn"));
        refreshBtn->setEnabled(true);
        refreshBtn->setMinimumSize(QSize(92, 25));
        refreshBtn->setMaximumSize(QSize(92, 25));
        refreshBtn->setCursor(QCursor(Qt::PointingHandCursor));
        refreshBtn->setStyleSheet(QString::fromUtf8(" QPushButton {\n"
"color: #94ffff;\n"
"border: 1px solid #888888;\n"
"font: 9pt \"\345\276\256\350\275\257\351\233\205\351\273\221\";\n"
"border-radius: 0px;\n"
"padding: 5px;\n"
"background: qradialgradient(cx: 0.3, cy: -0.4,\n"
"fx: 0.3, fy: -0.4,\n"
"radius: 0.57, stop: 0 #ffffff, stop: 1 #6cbcc8);\n"
"min-width: 80px;\n"
"}\n"
"\n"
"QPushButton:hover {\n"
"background: qradialgradient(cx: 0.3, cy: -0.4,\n"
"fx: 0.3, fy: -0.4,\n"
"radius: 0.57, stop: 0 #ffffff, stop: 1 #9bc3c8);\n"
"}\n"
"\n"
" QPushButton:pressed {\n"
"background: qradialgradient(cx: 0.4, cy: -0.1,\n"
"fx: 0.4, fy: -0.1,\n"
"radius: 0.57, stop: 0 #ffffff, stop: 1 #b1c6c8);\n"
"}\n"
"\n"
"QPushButton:disabled {\n"
"background: qradialgradient(cx: 0.4, cy: -0.1,\n"
"fx: 0.4, fy: -0.1,\n"
"radius: 0.57, stop: 0 #ffffff, stop: 1 #b1c6c8);\n"
"}"));

        horizontalLayout_4->addWidget(refreshBtn);

        horizontalSpacer_2 = new QSpacerItem(20, 20, QSizePolicy::Maximum, QSizePolicy::Minimum);

        horizontalLayout_4->addItem(horizontalSpacer_2);

        connectBtn = new QPushButton(connectWidget);
        connectBtn->setObjectName(QString::fromUtf8("connectBtn"));
        connectBtn->setMinimumSize(QSize(92, 25));
        connectBtn->setMaximumSize(QSize(92, 25));
        connectBtn->setCursor(QCursor(Qt::PointingHandCursor));
        connectBtn->setStyleSheet(QString::fromUtf8(" QPushButton {\n"
"color: #94ffff;\n"
"border: 1px solid #888888;\n"
"font: 9pt \"\345\276\256\350\275\257\351\233\205\351\273\221\";\n"
"border-radius: 0px;\n"
"padding: 5px;\n"
"background: qradialgradient(cx: 0.3, cy: -0.4,\n"
"fx: 0.3, fy: -0.4,\n"
"radius: 0.57, stop: 0 #ffffff, stop: 1 #6cbcc8);\n"
"min-width: 80px;\n"
"}\n"
"\n"
"QPushButton:hover {\n"
"background: qradialgradient(cx: 0.3, cy: -0.4,\n"
"fx: 0.3, fy: -0.4,\n"
"radius: 0.57, stop: 0 #ffffff, stop: 1 #9bc3c8);\n"
"}\n"
"\n"
" QPushButton:pressed {\n"
"background: qradialgradient(cx: 0.4, cy: -0.1,\n"
"fx: 0.4, fy: -0.1,\n"
"radius: 0.57, stop: 0 #ffffff, stop: 1 #b1c6c8);\n"
"}\n"
"\n"
"QPushButton:disabled {\n"
"background: qradialgradient(cx: 0.4, cy: -0.1,\n"
"fx: 0.4, fy: -0.1,\n"
"radius: 0.57, stop: 0 #ffffff, stop: 1 #b1c6c8);\n"
"}"));
        connectBtn->setCheckable(true);

        horizontalLayout_4->addWidget(connectBtn);


        gridLayout->addWidget(connectWidget, 2, 0, 1, 1);

        QWidget::setTabOrder(dataDisplayBtn, curvesDisplayBtn);
        QWidget::setTabOrder(curvesDisplayBtn, configBtn);
        QWidget::setTabOrder(configBtn, calibrateBtn);
        QWidget::setTabOrder(calibrateBtn, firmwareBtn);
        QWidget::setTabOrder(firmwareBtn, expandBtn);
        QWidget::setTabOrder(expandBtn, clearBtn);
        QWidget::setTabOrder(clearBtn, rateSelectBox);
        QWidget::setTabOrder(rateSelectBox, portSelectBox);
        QWidget::setTabOrder(portSelectBox, refreshBtn);
        QWidget::setTabOrder(refreshBtn, connectBtn);

        retranslateUi(SilverCoreUi);
        QObject::connect(dataDisplayBtn, SIGNAL(clicked()), SilverCoreUi, SLOT(pageSwitch()));
        QObject::connect(configBtn, SIGNAL(clicked()), SilverCoreUi, SLOT(pageSwitch()));
        QObject::connect(curvesDisplayBtn, SIGNAL(clicked()), SilverCoreUi, SLOT(pageSwitch()));
        QObject::connect(calibrateBtn, SIGNAL(clicked()), SilverCoreUi, SLOT(pageSwitch()));
        QObject::connect(firmwareBtn, SIGNAL(clicked()), SilverCoreUi, SLOT(pageSwitch()));
        QObject::connect(expandBtn, SIGNAL(clicked()), SilverCoreUi, SLOT(pageSwitch()));
        QObject::connect(clearBtn, SIGNAL(clicked()), SilverCoreUi, SLOT(clearCacheClicked()));
        QObject::connect(connectBtn, SIGNAL(clicked()), SilverCoreUi, SLOT(connectComClicked()));
        QObject::connect(rateSelectBox, SIGNAL(activated(QString)), SilverCoreUi, SLOT(selectSpeedChanged(QString)));
        QObject::connect(portSelectBox, SIGNAL(activated(QString)), SilverCoreUi, SLOT(selectPortChanged(QString)));
        QObject::connect(refreshBtn, SIGNAL(clicked()), SilverCoreUi, SLOT(refreshPortClicked()));

        dataDisplayBtn->setDefault(false);
        calibrateBtn->setDefault(false);
        firmwareBtn->setDefault(false);
        expandBtn->setDefault(false);
        rateSelectBox->setCurrentIndex(2);


        QMetaObject::connectSlotsByName(SilverCoreUi);
    } // setupUi

    void retranslateUi(QDialog *SilverCoreUi)
    {
        SilverCoreUi->setWindowTitle(QApplication::translate("SilverCoreUi", "SilverCore 2.1.5", nullptr));
        SilverCoreUi->setWindowFilePath(QString());
        dataDisplayBtn->setText(QApplication::translate("SilverCoreUi", "\346\225\260\346\215\256\346\230\276\347\244\272", nullptr));
        curvesDisplayBtn->setText(QApplication::translate("SilverCoreUi", "\346\263\242\345\275\242\346\230\276\347\244\272", nullptr));
        configBtn->setText(QApplication::translate("SilverCoreUi", "\346\250\241\345\235\227\350\256\276\347\275\256", nullptr));
        calibrateBtn->setText(QApplication::translate("SilverCoreUi", "\346\250\241\345\235\227\346\240\241\345\207\206", nullptr));
        firmwareBtn->setText(QApplication::translate("SilverCoreUi", "\345\233\272\344\273\266\346\233\264\346\226\260", nullptr));
        expandBtn->setText(QApplication::translate("SilverCoreUi", "\346\213\223\345\261\225\345\212\237\350\203\275", nullptr));
#ifndef QT_NO_SHORTCUT
        expandBtn->setShortcut(QString());
#endif // QT_NO_SHORTCUT
        tipsIconLabel->setText(QString());
        tipsTextLabel->setText(QString());
        rxGroupBox->setTitle(QString());
        RxLabel->setText(QApplication::translate("SilverCoreUi", "RX:", nullptr));
        RxNumLabel->setText(QApplication::translate("SilverCoreUi", "0", nullptr));
        txGroupBox->setTitle(QString());
        TxLabel->setText(QApplication::translate("SilverCoreUi", "TX:", nullptr));
        TxNumLabel->setText(QApplication::translate("SilverCoreUi", "0", nullptr));
        clearBtn->setText(QApplication::translate("SilverCoreUi", "\345\210\240\351\231\244\347\274\223\345\255\230", nullptr));
        speedGroupBox->setTitle(QString());
        speedLabel->setText(QApplication::translate("SilverCoreUi", "speed:", nullptr));
        rateSelectBox->setItemText(0, QApplication::translate("SilverCoreUi", "115200", nullptr));
        rateSelectBox->setItemText(1, QApplication::translate("SilverCoreUi", "230400", nullptr));
        rateSelectBox->setItemText(2, QApplication::translate("SilverCoreUi", "460800", nullptr));
        rateSelectBox->setItemText(3, QApplication::translate("SilverCoreUi", "921600", nullptr));
        rateSelectBox->setItemText(4, QApplication::translate("SilverCoreUi", "1382400", nullptr));

        portGroupBox->setTitle(QString());
        portLabel->setText(QApplication::translate("SilverCoreUi", "port:", nullptr));
        refreshBtn->setText(QApplication::translate("SilverCoreUi", "\345\210\267\346\226\260\350\256\276\345\244\207", nullptr));
        connectBtn->setText(QApplication::translate("SilverCoreUi", "\350\277\236\346\216\245\350\256\276\345\244\207", nullptr));
    } // retranslateUi

};

namespace Ui {
    class SilverCoreUi: public Ui_SilverCoreUi {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_SILVERCOREUI_H
