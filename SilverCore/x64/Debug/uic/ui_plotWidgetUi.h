/********************************************************************************
** Form generated from reading UI file 'plotWidgetUi.ui'
**
** Created by: Qt User Interface Compiler version 5.12.3
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_PLOTWIDGETUI_H
#define UI_PLOTWIDGETUI_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QScrollBar>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QSpinBox>
#include <QtWidgets/QSplitter>
#include <QtWidgets/QWidget>
#include <curvesplot.h>
#include <settingtable.h>
#include "combobox.h"

QT_BEGIN_NAMESPACE

class Ui_plotWidgetUi
{
public:
    QGridLayout *gridLayout;
    QSplitter *splitter_2;
    QWidget *leftUpWidget;
    QGridLayout *gridLayout_4;
    CurvesPlot *plotWidget;
    QScrollBar *curvesScrollBar;
    QWidget *rightUpWidget;
    QGridLayout *gridLayout_3;
    QGroupBox *showGroupBox;
    QGridLayout *gridLayout_2;
    QHBoxLayout *horizontalLayout_4;
    QPushButton *btnSelectAll;
    QSpacerItem *horizontalSpacer;
    QPushButton *btnSelectNone;
    QHBoxLayout *horizontalLayout_6;
    QHBoxLayout *horizontalLayout;
    QLabel *xRangeLabel;
    QSpinBox *xRangeSpinBox;
    QSpacerItem *horizontalSpacer_3;
    QPushButton *allDisplayButton;
    QHBoxLayout *horizontalLayout_7;
    QCheckBox *tracerCheckBox;
    QHBoxLayout *horizontalLayout_2;
    QLabel *tracerTargetLabel;
    ComboBox *tracerComboBox;
    QHBoxLayout *horizontalLayout_3;
    QLabel *searchLabel;
    QLineEdit *regExpInput;
    QCheckBox *showTipFlag;
    QHBoxLayout *horizontalLayout_5;
    QPushButton *showSwitchButton;
    QSpacerItem *horizontalSpacer_2;
    QPushButton *clearButton;
    SettingTable *settingTable;

    void setupUi(QWidget *plotWidgetUi)
    {
        if (plotWidgetUi->objectName().isEmpty())
            plotWidgetUi->setObjectName(QString::fromUtf8("plotWidgetUi"));
        plotWidgetUi->resize(1151, 680);
        QFont font;
        font.setFamily(QString::fromUtf8("\345\276\256\350\275\257\351\233\205\351\273\221"));
        plotWidgetUi->setFont(font);
        gridLayout = new QGridLayout(plotWidgetUi);
        gridLayout->setSpacing(0);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        gridLayout->setContentsMargins(0, 0, 0, 0);
        splitter_2 = new QSplitter(plotWidgetUi);
        splitter_2->setObjectName(QString::fromUtf8("splitter_2"));
        splitter_2->setOrientation(Qt::Horizontal);
        leftUpWidget = new QWidget(splitter_2);
        leftUpWidget->setObjectName(QString::fromUtf8("leftUpWidget"));
        QSizePolicy sizePolicy(QSizePolicy::Preferred, QSizePolicy::Preferred);
        sizePolicy.setHorizontalStretch(1);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(leftUpWidget->sizePolicy().hasHeightForWidth());
        leftUpWidget->setSizePolicy(sizePolicy);
        leftUpWidget->setAutoFillBackground(false);
        gridLayout_4 = new QGridLayout(leftUpWidget);
        gridLayout_4->setObjectName(QString::fromUtf8("gridLayout_4"));
        plotWidget = new CurvesPlot(leftUpWidget);
        plotWidget->setObjectName(QString::fromUtf8("plotWidget"));
        QSizePolicy sizePolicy1(QSizePolicy::Preferred, QSizePolicy::Preferred);
        sizePolicy1.setHorizontalStretch(1);
        sizePolicy1.setVerticalStretch(1);
        sizePolicy1.setHeightForWidth(plotWidget->sizePolicy().hasHeightForWidth());
        plotWidget->setSizePolicy(sizePolicy1);

        gridLayout_4->addWidget(plotWidget, 0, 0, 1, 1);

        curvesScrollBar = new QScrollBar(leftUpWidget);
        curvesScrollBar->setObjectName(QString::fromUtf8("curvesScrollBar"));
        curvesScrollBar->setOrientation(Qt::Horizontal);

        gridLayout_4->addWidget(curvesScrollBar, 1, 0, 1, 1);

        splitter_2->addWidget(leftUpWidget);
        rightUpWidget = new QWidget(splitter_2);
        rightUpWidget->setObjectName(QString::fromUtf8("rightUpWidget"));
        sizePolicy.setHeightForWidth(rightUpWidget->sizePolicy().hasHeightForWidth());
        rightUpWidget->setSizePolicy(sizePolicy);
        rightUpWidget->setMaximumSize(QSize(250, 16777215));
        gridLayout_3 = new QGridLayout(rightUpWidget);
        gridLayout_3->setObjectName(QString::fromUtf8("gridLayout_3"));
        showGroupBox = new QGroupBox(rightUpWidget);
        showGroupBox->setObjectName(QString::fromUtf8("showGroupBox"));
        QSizePolicy sizePolicy2(QSizePolicy::Preferred, QSizePolicy::Preferred);
        sizePolicy2.setHorizontalStretch(0);
        sizePolicy2.setVerticalStretch(0);
        sizePolicy2.setHeightForWidth(showGroupBox->sizePolicy().hasHeightForWidth());
        showGroupBox->setSizePolicy(sizePolicy2);
        showGroupBox->setMaximumSize(QSize(250, 200));
        showGroupBox->setCheckable(false);
        gridLayout_2 = new QGridLayout(showGroupBox);
        gridLayout_2->setObjectName(QString::fromUtf8("gridLayout_2"));
        horizontalLayout_4 = new QHBoxLayout();
        horizontalLayout_4->setSpacing(1);
        horizontalLayout_4->setObjectName(QString::fromUtf8("horizontalLayout_4"));
        btnSelectAll = new QPushButton(showGroupBox);
        btnSelectAll->setObjectName(QString::fromUtf8("btnSelectAll"));
        QSizePolicy sizePolicy3(QSizePolicy::Fixed, QSizePolicy::Fixed);
        sizePolicy3.setHorizontalStretch(0);
        sizePolicy3.setVerticalStretch(0);
        sizePolicy3.setHeightForWidth(btnSelectAll->sizePolicy().hasHeightForWidth());
        btnSelectAll->setSizePolicy(sizePolicy3);
        btnSelectAll->setMaximumSize(QSize(45, 25));
        btnSelectAll->setCursor(QCursor(Qt::PointingHandCursor));
        btnSelectAll->setStyleSheet(QString::fromUtf8(" QPushButton {\n"
"color: #94ffff;\n"
"border: 1px solid #888888;\n"
"font: 9pt \"\345\276\256\350\275\257\351\233\205\351\273\221\";\n"
"border-radius: 0px;\n"
"padding: 5px;\n"
"background: qradialgradient(cx: 0.3, cy: -0.4,\n"
"fx: 0.3, fy: -0.4,\n"
"radius: 0.57, stop: 0 #ffffff, stop: 1 #6cbcc8);\n"
"min-width: 80px;\n"
"}\n"
"\n"
"QPushButton:hover {\n"
"background: qradialgradient(cx: 0.3, cy: -0.4,\n"
"fx: 0.3, fy: -0.4,\n"
"radius: 0.57, stop: 0 #ffffff, stop: 1 #9bc3c8);\n"
"}\n"
"\n"
" QPushButton:pressed {\n"
"background: qradialgradient(cx: 0.4, cy: -0.1,\n"
"fx: 0.4, fy: -0.1,\n"
"radius: 0.57, stop: 0 #ffffff, stop: 1 #b1c6c8);\n"
"}\n"
"\n"
"QPushButton:disabled {\n"
"background: qradialgradient(cx: 0.4, cy: -0.1,\n"
"fx: 0.4, fy: -0.1,\n"
"radius: 0.57, stop: 0 #ffffff, stop: 1 #b1c6c8);\n"
"}"));

        horizontalLayout_4->addWidget(btnSelectAll);

        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_4->addItem(horizontalSpacer);

        btnSelectNone = new QPushButton(showGroupBox);
        btnSelectNone->setObjectName(QString::fromUtf8("btnSelectNone"));
        sizePolicy3.setHeightForWidth(btnSelectNone->sizePolicy().hasHeightForWidth());
        btnSelectNone->setSizePolicy(sizePolicy3);
        btnSelectNone->setMaximumSize(QSize(50, 25));
        btnSelectNone->setCursor(QCursor(Qt::PointingHandCursor));
        btnSelectNone->setStyleSheet(QString::fromUtf8(" QPushButton {\n"
"color: #94ffff;\n"
"border: 1px solid #888888;\n"
"font: 9pt \"\345\276\256\350\275\257\351\233\205\351\273\221\";\n"
"border-radius: 0px;\n"
"padding: 5px;\n"
"background: qradialgradient(cx: 0.3, cy: -0.4,\n"
"fx: 0.3, fy: -0.4,\n"
"radius: 0.57, stop: 0 #ffffff, stop: 1 #6cbcc8);\n"
"min-width: 80px;\n"
"}\n"
"\n"
"QPushButton:hover {\n"
"background: qradialgradient(cx: 0.3, cy: -0.4,\n"
"fx: 0.3, fy: -0.4,\n"
"radius: 0.57, stop: 0 #ffffff, stop: 1 #9bc3c8);\n"
"}\n"
"\n"
" QPushButton:pressed {\n"
"background: qradialgradient(cx: 0.4, cy: -0.1,\n"
"fx: 0.4, fy: -0.1,\n"
"radius: 0.57, stop: 0 #ffffff, stop: 1 #b1c6c8);\n"
"}\n"
"\n"
"QPushButton:disabled {\n"
"background: qradialgradient(cx: 0.4, cy: -0.1,\n"
"fx: 0.4, fy: -0.1,\n"
"radius: 0.57, stop: 0 #ffffff, stop: 1 #b1c6c8);\n"
"}"));

        horizontalLayout_4->addWidget(btnSelectNone);


        gridLayout_2->addLayout(horizontalLayout_4, 3, 0, 1, 1);

        horizontalLayout_6 = new QHBoxLayout();
        horizontalLayout_6->setObjectName(QString::fromUtf8("horizontalLayout_6"));
        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        xRangeLabel = new QLabel(showGroupBox);
        xRangeLabel->setObjectName(QString::fromUtf8("xRangeLabel"));
        xRangeLabel->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);

        horizontalLayout->addWidget(xRangeLabel);

        xRangeSpinBox = new QSpinBox(showGroupBox);
        xRangeSpinBox->setObjectName(QString::fromUtf8("xRangeSpinBox"));
        xRangeSpinBox->setMaximumSize(QSize(60, 16777215));
        xRangeSpinBox->setStyleSheet(QString::fromUtf8("QSpinBox\n"
"{\n"
"	border:1px solid #242424;\n"
"}\n"
" \n"
"QSpinBox::up-button,QSpinBox::down-button\n"
"{\n"
"	width:0px;\n"
"}"));
        xRangeSpinBox->setAlignment(Qt::AlignCenter);
        xRangeSpinBox->setMaximum(100000);
        xRangeSpinBox->setStepType(QAbstractSpinBox::DefaultStepType);
        xRangeSpinBox->setValue(5000);

        horizontalLayout->addWidget(xRangeSpinBox);

        horizontalSpacer_3 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer_3);

        allDisplayButton = new QPushButton(showGroupBox);
        allDisplayButton->setObjectName(QString::fromUtf8("allDisplayButton"));
        allDisplayButton->setMaximumSize(QSize(45, 25));
        allDisplayButton->setCursor(QCursor(Qt::PointingHandCursor));
        allDisplayButton->setStyleSheet(QString::fromUtf8(" QPushButton {\n"
"color: #94ffff;\n"
"border: 1px solid #888888;\n"
"font: 9pt \"\345\276\256\350\275\257\351\233\205\351\273\221\";\n"
"border-radius: 0px;\n"
"padding: 5px;\n"
"background: qradialgradient(cx: 0.3, cy: -0.4,\n"
"fx: 0.3, fy: -0.4,\n"
"radius: 0.57, stop: 0 #ffffff, stop: 1 #6cbcc8);\n"
"min-width: 80px;\n"
"}\n"
"\n"
"QPushButton:hover {\n"
"background: qradialgradient(cx: 0.3, cy: -0.4,\n"
"fx: 0.3, fy: -0.4,\n"
"radius: 0.57, stop: 0 #ffffff, stop: 1 #9bc3c8);\n"
"}\n"
"\n"
" QPushButton:pressed {\n"
"background: qradialgradient(cx: 0.4, cy: -0.1,\n"
"fx: 0.4, fy: -0.1,\n"
"radius: 0.57, stop: 0 #ffffff, stop: 1 #b1c6c8);\n"
"}\n"
"\n"
"QPushButton:disabled {\n"
"background: qradialgradient(cx: 0.4, cy: -0.1,\n"
"fx: 0.4, fy: -0.1,\n"
"radius: 0.57, stop: 0 #ffffff, stop: 1 #b1c6c8);\n"
"}"));

        horizontalLayout->addWidget(allDisplayButton);


        horizontalLayout_6->addLayout(horizontalLayout);


        gridLayout_2->addLayout(horizontalLayout_6, 1, 0, 1, 1);

        horizontalLayout_7 = new QHBoxLayout();
        horizontalLayout_7->setObjectName(QString::fromUtf8("horizontalLayout_7"));
        tracerCheckBox = new QCheckBox(showGroupBox);
        tracerCheckBox->setObjectName(QString::fromUtf8("tracerCheckBox"));
        tracerCheckBox->setMaximumSize(QSize(80, 16777215));

        horizontalLayout_7->addWidget(tracerCheckBox);

        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        tracerTargetLabel = new QLabel(showGroupBox);
        tracerTargetLabel->setObjectName(QString::fromUtf8("tracerTargetLabel"));

        horizontalLayout_2->addWidget(tracerTargetLabel);

        tracerComboBox = new ComboBox(showGroupBox);
        tracerComboBox->setObjectName(QString::fromUtf8("tracerComboBox"));
        tracerComboBox->setMinimumSize(QSize(100, 0));

        horizontalLayout_2->addWidget(tracerComboBox);


        horizontalLayout_7->addLayout(horizontalLayout_2);


        gridLayout_2->addLayout(horizontalLayout_7, 2, 0, 1, 1);

        horizontalLayout_3 = new QHBoxLayout();
        horizontalLayout_3->setObjectName(QString::fromUtf8("horizontalLayout_3"));
        searchLabel = new QLabel(showGroupBox);
        searchLabel->setObjectName(QString::fromUtf8("searchLabel"));
        searchLabel->setMaximumSize(QSize(30, 16777215));

        horizontalLayout_3->addWidget(searchLabel);

        regExpInput = new QLineEdit(showGroupBox);
        regExpInput->setObjectName(QString::fromUtf8("regExpInput"));
        regExpInput->setMaximumSize(QSize(120, 16777215));
        regExpInput->setStyleSheet(QString::fromUtf8(""));

        horizontalLayout_3->addWidget(regExpInput);

        showTipFlag = new QCheckBox(showGroupBox);
        showTipFlag->setObjectName(QString::fromUtf8("showTipFlag"));
        sizePolicy3.setHeightForWidth(showTipFlag->sizePolicy().hasHeightForWidth());
        showTipFlag->setSizePolicy(sizePolicy3);
        showTipFlag->setCheckable(true);
        showTipFlag->setChecked(true);

        horizontalLayout_3->addWidget(showTipFlag);


        gridLayout_2->addLayout(horizontalLayout_3, 4, 0, 1, 1);

        horizontalLayout_5 = new QHBoxLayout();
        horizontalLayout_5->setObjectName(QString::fromUtf8("horizontalLayout_5"));
        showSwitchButton = new QPushButton(showGroupBox);
        showSwitchButton->setObjectName(QString::fromUtf8("showSwitchButton"));
        showSwitchButton->setMaximumSize(QSize(16777215, 25));
        showSwitchButton->setCursor(QCursor(Qt::PointingHandCursor));
        showSwitchButton->setStyleSheet(QString::fromUtf8(" QPushButton {\n"
"color: #94ffff;\n"
"border: 1px solid #888888;\n"
"font: 9pt \"\345\276\256\350\275\257\351\233\205\351\273\221\";\n"
"border-radius: 0px;\n"
"padding: 5px;\n"
"background: qradialgradient(cx: 0.3, cy: -0.4,\n"
"fx: 0.3, fy: -0.4,\n"
"radius: 0.57, stop: 0 #ffffff, stop: 1 #6cbcc8);\n"
"min-width: 80px;\n"
"}\n"
"\n"
"QPushButton:hover {\n"
"background: qradialgradient(cx: 0.3, cy: -0.4,\n"
"fx: 0.3, fy: -0.4,\n"
"radius: 0.57, stop: 0 #ffffff, stop: 1 #9bc3c8);\n"
"}\n"
"\n"
" QPushButton:pressed {\n"
"background: qradialgradient(cx: 0.4, cy: -0.1,\n"
"fx: 0.4, fy: -0.1,\n"
"radius: 0.57, stop: 0 #ffffff, stop: 1 #b1c6c8);\n"
"}\n"
"\n"
"QPushButton:disabled {\n"
"background: qradialgradient(cx: 0.4, cy: -0.1,\n"
"fx: 0.4, fy: -0.1,\n"
"radius: 0.57, stop: 0 #ffffff, stop: 1 #b1c6c8);\n"
"}"));

        horizontalLayout_5->addWidget(showSwitchButton);

        horizontalSpacer_2 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_5->addItem(horizontalSpacer_2);

        clearButton = new QPushButton(showGroupBox);
        clearButton->setObjectName(QString::fromUtf8("clearButton"));
        clearButton->setMaximumSize(QSize(16777215, 25));
        clearButton->setCursor(QCursor(Qt::PointingHandCursor));
        clearButton->setStyleSheet(QString::fromUtf8(" QPushButton {\n"
"color: #94ffff;\n"
"border: 1px solid #888888;\n"
"font: 9pt \"\345\276\256\350\275\257\351\233\205\351\273\221\";\n"
"border-radius: 0px;\n"
"padding: 5px;\n"
"background: qradialgradient(cx: 0.3, cy: -0.4,\n"
"fx: 0.3, fy: -0.4,\n"
"radius: 0.57, stop: 0 #ffffff, stop: 1 #6cbcc8);\n"
"min-width: 80px;\n"
"}\n"
"\n"
"QPushButton:hover {\n"
"background: qradialgradient(cx: 0.3, cy: -0.4,\n"
"fx: 0.3, fy: -0.4,\n"
"radius: 0.57, stop: 0 #ffffff, stop: 1 #9bc3c8);\n"
"}\n"
"\n"
" QPushButton:pressed {\n"
"background: qradialgradient(cx: 0.4, cy: -0.1,\n"
"fx: 0.4, fy: -0.1,\n"
"radius: 0.57, stop: 0 #ffffff, stop: 1 #b1c6c8);\n"
"}\n"
"\n"
"QPushButton:disabled {\n"
"background: qradialgradient(cx: 0.4, cy: -0.1,\n"
"fx: 0.4, fy: -0.1,\n"
"radius: 0.57, stop: 0 #ffffff, stop: 1 #b1c6c8);\n"
"}"));

        horizontalLayout_5->addWidget(clearButton);


        gridLayout_2->addLayout(horizontalLayout_5, 0, 0, 1, 1);


        gridLayout_3->addWidget(showGroupBox, 0, 0, 1, 1);

        settingTable = new SettingTable(rightUpWidget);
        settingTable->setObjectName(QString::fromUtf8("settingTable"));
        settingTable->setStyleSheet(QString::fromUtf8(""));

        gridLayout_3->addWidget(settingTable, 1, 0, 1, 1);

        splitter_2->addWidget(rightUpWidget);

        gridLayout->addWidget(splitter_2, 0, 0, 1, 1);

        QWidget::setTabOrder(showSwitchButton, clearButton);
        QWidget::setTabOrder(clearButton, xRangeSpinBox);
        QWidget::setTabOrder(xRangeSpinBox, allDisplayButton);
        QWidget::setTabOrder(allDisplayButton, tracerCheckBox);
        QWidget::setTabOrder(tracerCheckBox, tracerComboBox);
        QWidget::setTabOrder(tracerComboBox, btnSelectAll);
        QWidget::setTabOrder(btnSelectAll, btnSelectNone);
        QWidget::setTabOrder(btnSelectNone, regExpInput);
        QWidget::setTabOrder(regExpInput, showTipFlag);
        QWidget::setTabOrder(showTipFlag, settingTable);

        retranslateUi(plotWidgetUi);
        QObject::connect(btnSelectAll, SIGNAL(clicked()), plotWidgetUi, SLOT(selectAll()));
        QObject::connect(btnSelectNone, SIGNAL(clicked()), plotWidgetUi, SLOT(selectNone()));
        QObject::connect(xRangeSpinBox, SIGNAL(editingFinished()), plotWidgetUi, SLOT(xAxisChangeRange()));
        QObject::connect(showSwitchButton, SIGNAL(clicked()), plotWidgetUi, SLOT(showSwitch()));
        QObject::connect(tracerCheckBox, SIGNAL(clicked(bool)), plotWidgetUi, SLOT(tracerClicked(bool)));
        QObject::connect(regExpInput, SIGNAL(textChanged(QString)), plotWidgetUi, SLOT(curvesSearch(QString)));
        QObject::connect(tracerComboBox, SIGNAL(currentIndexChanged(int)), settingTable, SLOT(selectRow(int)));
        QObject::connect(showTipFlag, SIGNAL(clicked(bool)), plotWidgetUi, SLOT(numberUpdate(bool)));

        QMetaObject::connectSlotsByName(plotWidgetUi);
    } // setupUi

    void retranslateUi(QWidget *plotWidgetUi)
    {
        plotWidgetUi->setWindowTitle(QApplication::translate("plotWidgetUi", "\346\263\242\345\275\242\345\233\276", nullptr));
        showGroupBox->setTitle(QApplication::translate("plotWidgetUi", "\346\230\276\347\244\272\350\256\276\347\275\256", nullptr));
        btnSelectAll->setText(QApplication::translate("plotWidgetUi", "\345\205\250\351\200\211", nullptr));
        btnSelectNone->setText(QApplication::translate("plotWidgetUi", "\345\205\250\344\270\215\351\200\211", nullptr));
        xRangeLabel->setText(QApplication::translate("plotWidgetUi", "\346\230\276\347\244\272\350\214\203\345\233\264", nullptr));
        allDisplayButton->setText(QApplication::translate("plotWidgetUi", "\345\205\250\346\230\276", nullptr));
        tracerCheckBox->setText(QApplication::translate("plotWidgetUi", "\346\270\270\346\240\207", nullptr));
        tracerTargetLabel->setText(QApplication::translate("plotWidgetUi", "\350\267\237\351\232\217\346\233\262\347\272\277", nullptr));
        searchLabel->setText(QApplication::translate("plotWidgetUi", "\346\220\234\347\264\242\357\274\232", nullptr));
#ifndef QT_NO_TOOLTIP
        showTipFlag->setToolTip(QApplication::translate("plotWidgetUi", "\351\200\211\344\270\255\345\220\216\345\217\257\345\256\236\346\227\266\346\230\276\347\244\272\346\234\200\346\226\260\345\200\274", nullptr));
#endif // QT_NO_TOOLTIP
        showTipFlag->setText(QApplication::translate("plotWidgetUi", "\346\225\260\345\200\274", nullptr));
        showSwitchButton->setText(QApplication::translate("plotWidgetUi", "\345\274\200\345\220\257\346\230\276\347\244\272", nullptr));
        clearButton->setText(QApplication::translate("plotWidgetUi", "\346\270\205\351\231\244\346\230\276\347\244\272", nullptr));
    } // retranslateUi

};

namespace Ui {
    class plotWidgetUi: public Ui_plotWidgetUi {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_PLOTWIDGETUI_H
