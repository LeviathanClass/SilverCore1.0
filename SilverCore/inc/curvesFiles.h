#pragma once

#include <QtWidgets/QWidget>
#include <ui_curvesFilesUi.h>
#include <curvesPlot.h>
#include <qFlowLayout.h>


#if defined(_MSC_VER) && (_MSC_VER >= 1600)
# pragma execution_character_set("utf-8")
#endif

class SaveRange {
public:
	SaveRange(GraphData *curve) :
		graphData(curve) {
		lowKey = graphData->keyVec.first();
		highKey = graphData->keyVec.last();
		lowKeyIndex = 0;
		highKeyIndex = graphData->keyVec.end() - graphData->keyVec.begin();
		dataCount = graphData->keyVec.count();
	}
public:
	GraphData *graphData;
	double lowKey;	//从曲线中读到键的低区间，用于lowRangeIntegerSpin
	double highKey;	//从曲线中读到键的高区间，用于highRangeIntegerSpin
	int lowKeyIndex;
	int highKeyIndex;
	int dataCount;
};

class CurvesFiles :
	public QDialog {
	Q_OBJECT
public:
	CurvesFiles(QVector<GraphData> *graphData);
	~CurvesFiles();

private slots:
	void checkBoxUpdate(bool);
	void saveFiles();
	void selectedCurveChanged(int);
	void checkCurveSpinRange(void);

private:
	void getSelectedCurveRange(int selected);
	void saveCsv(QString fileName);

private:
	Ui::curvesFilesUi ui;
	QFlowLayout *_flowLayout;
	QVector<GraphData> *_graphData;
	QMap<QCheckBox *, SaveRange *> _curveMap;
	QList<SaveRange *> _saveRangeList;
	SaveRange *_currentSaveRange;
};