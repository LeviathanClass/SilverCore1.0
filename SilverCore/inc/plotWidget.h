﻿#pragma once
#include <QtWidgets/QWidget>
#include <ui_plotWidgetUi.h>
#include <QtCore/QThread>
#include <QSettings>
#include "qcustomplot.h"
#include "curvesPlot.h"
#include "CurvesFiles.h"
#include <QtAwesome.h>

#if defined(_MSC_VER) && (_MSC_VER >= 1600)
# pragma execution_character_set("utf-8")
#endif

class PlotWidget :
	public QWidget {
	Q_OBJECT

public:
	PlotWidget(QSettings *settings);
	~PlotWidget();

protected:
	void timerEvent(QTimerEvent *event);
	void closeEvent(QCloseEvent *event);

signals:
	void operationRecord(fa::icon icon, QString record);
	void clearCurves();

private slots:
	void setCurvesReload(bool);
	void xAxisChangeRange();
	void tracerClicked(bool checked);
	void selectShowChange(bool checked);
	void showSwitch();
	void xAxisCangeCallBack(QCPRange range);
	void selectNone();
	void selectAll();
	void curveColorChanged(QColor color);
	void curveNameChanged(QString name);
	void traceTargetChanged();
	void curvesSearch(const QString &text);
	void numberUpdate(bool checked);

	void curvesScrollBarChanged(int value);
	void curvesScrollBarAction(int value);
	void curvesXAxisChanged(QCPRange range);
	void fileOperation();

private:
	Ui::plotWidgetUi ui;
	CurvesPlot *curves;
	bool _numberDisplay;
	//config
	QSettings *_settings;
	CurvesFiles *_curvesFiles;
};

