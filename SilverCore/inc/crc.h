﻿#pragma once

#include <QtCore/qmath.h>

class CRC {
public:
	
	static uint8_t VerifyCrc8CheckSum(uint8_t *pchMessage, uint32_t dwLength);
	static void AppendCrc8CheckSum(uint8_t *pchMessage, uint32_t dwLength);
	
	static uint8_t VerifyCrc16CheckSum(uint8_t *pchMessage, uint32_t dwLength);
	static void AppendCrc16CheckSum(uint8_t *pchMessage, uint32_t dwLength);

	static uint8_t VerifyCrc32CheckSum(uint8_t *pchMessage, uint32_t dwLength);
	static void AppendCrc32CheckSum(uint8_t *pchMessage, uint32_t dwLength);
private:
	static uint8_t GetCrc8CheckSum(uint8_t *pchMessage, uint32_t dwLength, uint8_t crc);
	static uint16_t GetCrc16CheckSum(uint8_t *pchMessage, uint32_t dwLength, uint16_t crc);
	static uint32_t GetCrc32CheckSum(uint8_t *pchMessage, uint32_t dwLength, uint32_t crc);
};
