﻿#pragma once
#include <QtWidgets/QWidget>
#include <ui_expandWidgetUi.h>
#include <QtAwesome.h>

#if defined(_MSC_VER) && (_MSC_VER >= 1600)
# pragma execution_character_set("utf-8")
#endif

class ExpandWidget :
	public QWidget {
	Q_OBJECT
public:
	ExpandWidget(QWidget *parent = 0);
	~ExpandWidget();
signals:
	void operationRecord(fa::icon icon, QString record);
private slots:

private:
	Ui::expandWidgetUi ui;
};
