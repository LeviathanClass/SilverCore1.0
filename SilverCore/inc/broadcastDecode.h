﻿#pragma once

#include <QObject>
#include <QtCore/qmath.h>
#include "app.h"
#include "byteConverter.h"

//帧类型
typedef enum {
	FRAME_TYPE_SENSOR = 0x01,
	FRAME_TYPE_FORWARD0,
	FRAME_TYPE_FORWARD1,
	FRAME_TYPE_LOG,
} FRAME_TYPE_E;

//传感器数据包类型下标
typedef enum {
	SENSOR_GYRO_INDEX = 0x00,
	SENSOR_ACCEL_INDEX = 0x03,
	SENSOR_MAG_INDEX = 0x06,
	SENSOR_PRES_INDEX = 0x09,
	SENSOR_EULER_INDEX = 0x0A,
	SENSOR_QUAT_INDEX = 0x0D,
	SENSOR_TEMP_INDEX = 0x11,
} SENSOR_BASE_INDEX_E;

//数据的处理
class BroadcastDecode : public QObject {
	Q_OBJECT
public:
	BroadcastDecode();
	~BroadcastDecode();

	void decode(uint8_t *src, uint16_t length);

private:
	void pushToList();
	void sensorPacketDecode(uint8_t *src, uint16_t length);
	void logPacketDecode(uint8_t *src, uint16_t length);

signals:
	void stringDecode(QString str);

private:
	uint16_t _currentCNTR;
	QVector<SensorCache> _sensorCache;
	QString log;
};

