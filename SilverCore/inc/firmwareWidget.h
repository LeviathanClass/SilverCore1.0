﻿#pragma once
#include <QtWidgets/QWidget>
#include <ui_firmwareWidgetUi.h>
#include <git2.h>
#include <io.h>
#include <iostream>
#include <fstream>
#include <direct.h>
#include "config.h"
#include "connectThread.h"
#include <QtAwesome.h>

#if defined(_MSC_VER) && (_MSC_VER >= 1600)
# pragma execution_character_set("utf-8")
#endif

#define MAX_PACKET_LENGTH	2048

class FirmwareWidget :
	public QWidget {
	Q_OBJECT
public:
	FirmwareWidget(ConnectThread *thread, Config *config);
	~FirmwareWidget();
private:
	void createJsonControlMapping();
	void firmwareRecord(fa::icon icon, QString record);
	void getRemoteBranchList(bool isAll);
	void setFirmwareCache();
	void autoCreateDir(QString dir);
	void sendNextBinFrame();
	void resendBinFrame();
	void manualSelectBin();
	void downLoad();
signals:
	void operationRecord(fa::icon icon, QString record);
	void reConnectPort(bool enable, bool init);
private slots:
	void appVersionDecode(JsonDirContent content);
	void bootVersionDecode(JsonDirContent content);
	void setGetVersionEnable(bool enable);
	void downloadStringDecode(QByteArray str);
	void firmwareClicked();
	void autoUpdateClicked(bool clicked);
	void selectFirmware(QModelIndex model);
protected:
	void timerEvent(QTimerEvent *event) Q_DECL_OVERRIDE;
private:
	Ui::firmwareWidgetUi ui;
	QtAwesome* _awesome;
	Config *_config;
	ConnectThread *_connectThread;
	QList<QString> _branchList;
	QString _selectBranch;
	QString _newestBranch;
	git_repository *_rep;
	git_remote *_remote;

	QString _modelType;
	QString _bootVersion;
	QString _appVersion;
	
	uint32_t _currentBinNumber;
	uint32_t _totalBinLength;
	uint32_t _partLength;
	QByteArray _binStream;
	QByteArray _currentSendFrame;
	uint32_t _nCountBinNumber;

	QString _reconnectPortName;
	qint32 _reconnectBaudrate;
	int _startDownloadTimerId;
	int _reconnectTimerId;
};

