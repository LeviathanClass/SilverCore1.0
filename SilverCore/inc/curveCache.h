#pragma once

#include <QtCore/QVector>

//单个曲线的值信息
class CurveValue {
public:
	uint16_t order;	//曲线序号
	double value;	//曲线值(y)
};

//所有曲线的键信息，其中包含n条曲线的value
class CurveKey {
public:
	double key;	//曲线键(x)
	QVector<CurveValue> valueCache;	//曲线值向量
};

class CurveConfig {
public:
	QString name;	//曲线名称
	double yMax;	//曲线y最大值
	double yMin;	//曲线y最小值
	bool isShow;	//曲线是否显示
};

class CurveCache {
public:
	//构造函数
	CurveCache(uint16_t count = 0, double nowKey = 0);
	//析构函数
	~CurveCache();

	//获取曲线数量
	uint16_t getCount() {
		return _count;
	}

	//设置缓存中某条曲线的名称
	void setName(uint16_t i, QString name);
	//获取缓存中某条曲线的名称
	QString getName(uint16_t i);

	//设置缓存中某条曲线是否显示
	void setShow(uint16_t i, bool show);
	//获取缓存中某条曲线是否显示
	bool isShow(uint16_t i);

	//更新当前时刻下所有曲线的键
	void keyUpdate();

	//向当前最新的键缓存中存入曲线值
	void addLastValue(uint16_t i, double value);

	//向某一个键缓存中存入曲线值
	void addValue(double key, uint16_t i, double value);

	//获取最后一个键中的缓存数据
	CurveKey getLastCache();

	//获取某一个键中的缓存数据
	CurveKey getValue(double key);

private:
	uint16_t _count;				//曲线的数量
	QVector<CurveConfig> *_curvesConfig;	//曲线的配置信息
	double _nowKey;
	QVector<CurveKey> *_keyVector;	//曲线缓存向量，可用于保存文件
};

