﻿#pragma once

#include <QtWidgets/QDialog>
#include <QtSerialPort/QSerialPort>
#include <QtSerialPort/QSerialPortInfo>
#include <QSettings>
#include <QtAwesome.h>
#include "ui_SilverCoreUi.h"
#include "dataWidget.h"
#include "plotWidget.h"
#include "configWidget.h"
#include "calibrateWidget.h"
#include "firmwareWidget.h"
#include "expandWidget.h"
#include <config.h>
#include <connectThread.h>

#if defined(_MSC_VER) && (_MSC_VER >= 1600)
# pragma execution_character_set("utf-8")
#endif

class SilverCore : public QDialog {
	Q_OBJECT

public:
	SilverCore(QWidget *parent = Q_NULLPTR);
	~SilverCore();

signals:
	void setConnectControlEnable(bool enable);

private slots:
	void operationRecord(fa::icon icon, QString record);
	void pageSwitch();
	void clearCacheClicked();
	void refreshPortClicked();
	void connectComClicked();
	void selectSpeedChanged(QString rate);
	void selectPortChanged(QString port);
	void handleSerialError(QString tips);
	void displayTxNum(uint64_t length);
	void displayRxNum(uint64_t length);
	void connectPort(bool enable, bool init);

protected:
	void keyPressEvent(QKeyEvent *event) Q_DECL_OVERRIDE;

private:
	//开发者模式的控件映射
	void createJsonControlMapping();
	void InitMainWindowConfig();
	void setNavBtnIcon(NavButton *btn, int character);
private:
	//ui
	Ui::SilverCoreUi ui;
	QtAwesome* _awesome;
	//widget
	DataWidget *_dataWidget;
	PlotWidget *_plotWidget;
	ConfigWidget *_configWidget;
	CalibrateWidget *_calibrateWidget;
	FirmwareWidget *_firmwareWidget;
	ExpandWidget *_expandWidget;
	//port
	QSerialPort *_serialport;
	QString _portName;
	qint32 _baudrate;
	//connect
	ConnectThread *_connectThread;
	//config
	Config *_config;	
	QSettings *_settings;
};
