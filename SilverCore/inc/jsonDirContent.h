﻿#pragma once

#include <QObject>
#include <QWidget>
#include <dirTree.h>
#include <QStringList>
#include <jsonConversion.h>

class JsonDirContent : public QObject {
	Q_OBJECT
public:
	enum ConnectType{
		disConnect,
		activedResponse,
		passiveResponse,
		allResponse
	};

	//这是一次深拷贝，拷贝后dst会丢失原有的signals绑定
	JsonDirContent(const Tree T);
	JsonDirContent(const JsonDirContent &content);
	JsonDirContent(const QString key = "", const void * value = nullptr, const QJsonValue::Type type = QJsonValue::Null, const QWidget * control = nullptr, const ConnectType connect = ConnectType::disConnect, const QString dir = nullptr);
	~JsonDirContent();
	//获取键
	QString getKey() {
		return _key;
	}
	//设置键
	void setKey(QString key) {
		_key = key;
	}
	//获取值类型
	QJsonValue::Type getType() {
		return _type;
	}
	//设置值类型
	void setType(QJsonValue::Type type) {
		_type = type;
	}
	//获取值地址
	void *getValue() {
		return _value;
	}
	//设置值地址
	void setValue(void *value) {
		_value = value;
	}
	//获取路径
	QString getDir() {
		return _dir;
	}
	//设置路径
	void setDir(QString dir) {
		_dir = dir;
	}
	//获取控件地址
	QWidget *getControl() {
		return _control;
	}
	//设置控件地址
	void setControl(QWidget *control) {
		_control = control;
	}
	//获取控件连接方式
	ConnectType getConnectType() {
		return _connect;
	}
	//设置控件连接方式
	void setControlType(ConnectType connect) {
		_connect = connect;
	}
signals:
	void valueChanged(JsonDirContent content);
private:
	QString _key;	//键
	void *_value;	//值
	QJsonValue::Type _type;	//值类型
	QWidget *_control;	//对应控件
	ConnectType _connect;	//控件连接类型
	QString _dir;	//目录信息
};
