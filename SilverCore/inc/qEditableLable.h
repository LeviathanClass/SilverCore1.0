﻿#pragma once

#include <QtWidgets/QStackedWidget>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtGui/QMouseEvent>
#include <QtWidgets/QAction>

#if defined(_MSC_VER) && (_MSC_VER >= 1600)
# pragma execution_character_set("utf-8")
#endif

class QEditableLable :
	public QStackedWidget {
	Q_OBJECT
public:
	QEditableLable(QWidget *parent, QColor color, QString name);
	~QEditableLable();

	void setText(QString name);
	void setColor(QColor color);

	QColor curveColor;//曲线的颜色
	QString curveName;//曲线的名称

private:
	bool eventFilter(QObject*, QEvent*);
private slots:
	void modifyName();

signals:
	void colorChanged(QColor color);//颜色变更后的信号
	void nameChanged(QString name);//名称变更后的信号

private:
	QAction *actColor;//右键菜单:修改名称
	QAction *actName;//右键菜单:修改颜色
	QColor lastColor;
	QString lastName;
	QHBoxLayout *horizontalLayout;
	QHBoxLayout *horizontalLayout_1;
	QWidget *page;
	QWidget *page_1;
	QLabel *label;
	QLineEdit *lineEdit;
};

