﻿#pragma once
#include <QJsonObject>
#include <QJsonDocument>
#include <QJsonArray>

class JsonConversion {
public:
	//QString >> QJsonObject
	static QJsonObject getJsonObjectFromString(const QString jsonString);
	//QString >> QJsonArray
	static QJsonArray getJsonArrayFromString(const QString jsonString);
	//QString >> QByteArray
	static QByteArray getByteArrayFromString(const QString& jsonString);
	//QJsonObject >> QByteArray
	static QByteArray getByteArrayFromJsonObject(const QJsonObject& jsonObject);
	//QJsonObject >> QJsonArray
	static QJsonArray getJsonArrayFromJsonObject(const QJsonObject& jsonObject);
	//QJsonObject >> QString
	static QString getStringFromJsonObject(const QJsonObject& jsonObject);
	//QJsonArray >> QByteArray
	static QByteArray getByteArrayFromJsonArray(const QJsonArray& jsonArray);
	//QJsonArray >> QJsonObject
	static QJsonObject getJsonObjectFromJsonArray(const QJsonArray& jsonArray);
	//QJsonArray >> QString
	static QString getStringFromJsonArray(const QJsonArray& jsonArray);
	//QByteArray >> QJsonArray
	static QJsonArray getJsonArrayFromByteArray(const QByteArray& jsonByte);
	//QByteArray >> QJsonObject
	static QJsonObject getJsonObjectFromByteArray(const QByteArray& jsonByte);
	//QByteArray >> QString
	static QString getStringFromByteArray(const QByteArray& jsonByte);
};