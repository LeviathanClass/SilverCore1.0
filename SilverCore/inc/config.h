﻿#pragma once
#include <QObject>
#include <QtWidgets/QWidget>
#include <SwitchButton.h>
#include <ComboBox.h>
#include <QSpinBox>
#include <QDoubleSpinBox>
#include <QLabel>
#include <app.h>
#include <byteConverter.h>
#include <jsonConversion.h>
#include <jsonDirContent.h>
#include <jsonDecode.h>
#include <connectThread.h>


#if defined(_MSC_VER) && (_MSC_VER >= 1600)
# pragma execution_character_set("utf-8")
#endif

class Config : public QObject {
	Q_OBJECT
public:
	Config(ConnectThread *connectThread);
	~Config();
public:
	enum ConfigCommand {
		JsonConfig = 0x7F,
		ForwardData0,
		ForwardData1,
        CommandError = 0xFF
	};
	//给对应路径下的成员添加控件
	void setJsonDirControl(QString path, QWidget *control, JsonDirContent::ConnectType connectType);
	//对所有路径下的控件连接使能进行操作
	void connectAllLeaves(Tree T, bool enable);
	//在对应路径下的叶成员变更值后增加槽
	void addJsonDirSlot(QString path, const QObject *receive, const char *member);
	//设置对应路径下成员的值
	void sendJsonDirValue(QString path, void *value);
	//获取对应路径下成员的值
	void getJsonDirValue(QString path);

public:

	QMap<QWidget *, Tree> activedMapping;	//根据控件找到对应的树

private slots:
	void jsonStringDecode(QString str);
	void controlPassiveUpdate(JsonDirContent content);
	void swtBtnActive(bool checked);
	void comboBoxActive(QString selected);
	void integerSpinActive();
	void doubleSpinActive();

private:
	static QByteArray sendConfigCommand(ConfigCommand cmd, QByteArray data);
	void controlActiveUpdate(JsonDirContent *content, bool enable);

private:
	ConnectThread *_connectThread;

	Tree _root;	//根目录

	//测试
	bool _uartMsg;
	bool _canMsg;
	bool _usbMsg;
	bool _gyroMsg;
	bool _accelMsg;
	bool _magMsg;
	bool _presMsg;
	bool _attitudeMsg;
	bool _accelFusion;
	bool _magFusion;

	bool _usbCanTrans;
	bool _usbUartTrans;
	bool _uartCanTrans;

	bool _yawContinus;
	bool _gyroType;
	bool _accelType;
	bool _attitudeType;
	bool _heightType;
	bool _gyroLength;
	bool _accelLength;
	bool _magLength;
	bool _attitudeLength;

	QString _imuType;
	double _gyroODR;
	double _accelODR;
	double _attitudeODR;
	double _magODR;
	double _heightODR;
	double _gyroFSR;
	double _accelFSR;

	double _setGroup;
	double _baudrate;
	double _broadcastID;
	double _receiveID;

	double _installBias[3];

	QString _gyroAllan;
	QString _sixFace;
	QString _accelEllipsoid;
	QString _magEllipsoid;
	double _gyroBias[3];
	double _accelBias[3];
	double _magBias[3];

	bool _reset;
	bool _restore;
	bool _save;
	bool _update;
	bool _develop;
	double _bootVersion;
	double _appVersion;

	double _tempP;
	double _tempI;
	double _tempD;
	double _tempF;
	double _tempPM;
	double _tempIM;
	double _tempDM;
	double _tempOM;

	double _ukfAccelBiasQ;
	double _ukfGyroBiasQ;
	double _ukfQuatQ;
	double _ukfAccelBiasV;
	double _ukfGyroBiasV;
	double _ukfRateV;
	double _ukfAccN;
	double _ukfDistN;
	double _ukfMagN;
};
