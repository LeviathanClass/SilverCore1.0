﻿#pragma once

#include <QtCore/qmath.h>
#include <QtCore/QByteArray>
#include "crc.h"
#include "broadcastDecode.h"

//回环数组
class CircularArray
{
public:
	CircularArray(BroadcastDecode *analysis, uint8_t _frameHead, uint8_t _crc8Index, uint8_t _lengthIndex);
	~CircularArray();
	void AddData(uint8_t *data, int length);//追加数据到回环数组
	void AddData(QByteArray *data);
	void CopyFromArray(uint8_t *dst, uint16_t length);//从回环数组中拷贝数据，必须确保指针有足够的长度
	void DecodeData();//解析数据
	uint16_t GetHeader() { return header; }//返回header
	uint16_t GetTail() { return tail; }//返回tail
	uint16_t Length()
	{
		return (uint16_t)(tail - header);
	}
	int GetDecodeNum() { return decodeNum; }//返回decodePacket
private:
	BroadcastDecode *_analysis;
	uint16_t header;
	uint16_t tail;
	uint8_t buffer[65536];
	int decodeNum;
	uint8_t frameHead;
	uint8_t crc8Index;
	uint8_t lengthIndex;
};