﻿#ifndef AXISTAG_H
#define AXISTAG_H

#include <QObject>
#include "qcustomplot.h"

class AxisTag : public QObject
{
  Q_OBJECT
public:
  explicit AxisTag(QCPAxis *xAxis, QCPAxis *yAxis);
  virtual ~AxisTag();
  
  // setters:
  void setPen(const QPen &pen);
  void setColor(const QColor &color);
  void setBrush(const QBrush &brush);
  void setText(const QString &xText, const QString &yText);
  void setVisible(bool visible);
  void setGraph(QCPGraph *graph);

  // getters:
  QPen xPen() const { return xLabel->pen(); }
  QPen yPen() const { return yLabel->pen(); }
  QBrush xBrush() const { return xLabel->brush(); }
  QBrush yBrush() const { return yLabel->brush(); }
  QString xText() const { return xLabel->text(); }
  QString yText() const { return yLabel->text(); }
  
  // other methods:
  void updatePosition(QCPGraph * graph, double x);
  
protected:
  QCPAxis *_xAxis;
  QCPAxis *_yAxis;
  QPointer<QCPItemTracer> tracer;
  QPointer<QCPItemLine> xArrow;
  QPointer<QCPItemText> xLabel;
  QPointer<QCPItemLine> yArrow;
  QPointer<QCPItemText> yLabel;
};


#endif // AXISTAG_H
