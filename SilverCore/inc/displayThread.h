﻿#pragma once

#include <QtCore/QThread>
#include <QtCore/QMutex>

class DisplayThread :
	public QThread {
	Q_OBJECT

public:
	DisplayThread();
	volatile bool stoped;
	void stop();
	void run();
	void setRxDisplay(bool enabled) { 
		rxDisplay = enabled;
	}
	void setTxDisplay(bool enabled) {
		txDisplay = enabled;
	}
	uint64_t getRxLength() {
		return rxDisplayLength;
	}
	void setRxLength(uint64_t num) {
		rxDisplayLength = num;
	}
	uint64_t getTxLength() {
		return txDisplayLength;
	}
	void setTxLength(uint64_t num) {
		txDisplayLength = num;
	}

signals:
	void displayRxText(const QString &a);
	void displayTxText(const QString &a);
	void rxDisplayNum(uint64_t);
	void txDisplayNum(uint64_t);

private:
	bool rxDisplay;
	bool txDisplay;
	uint64_t rxDisplayLength;
	uint64_t txDisplayLength;
};

