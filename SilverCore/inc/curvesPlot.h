﻿#pragma once

#include "qcustomplot.h"
#include "axistag.h"
#include <QtGui/QMouseEvent>
#include <QtCore/QMap>
#include "curveCache.h"
#include "app.h"

#if defined(_MSC_VER) && (_MSC_VER >= 1600)
# pragma execution_character_set("utf-8")
#endif


//曲线信息
class GraphData {
public:
	bool isShow;
	bool reloadKey;
	uint8_t graphIndex;
	uint8_t valueLength;
	uint16_t lastCNTR;
	QString name;
	QColor color;
	QVector<double> keyVec;//x
	QVector<double> valVec;//y
	double yMax;
	double yMin;
};

class CurvesPlot :
	public QCustomPlot {
	Q_OBJECT
public:
	CurvesPlot(QWidget *parent = 0, int curvesCnt = 32);
	~CurvesPlot();
	void reloadBasicKey();
	void reloadGraphKey(uint8_t index);
	void setUpdate(bool enable){ displayUpdate = enable; }//是否打开显示
	bool isDisplayUpdate() { return displayUpdate; }//获取是否打开显示
	bool istracerValid() { return tracerValid; }//获取是否打开游标
	bool isAutoScroll() { return autoScroll; }//获取是否自动跟随
	void setAutoScroll(bool enable) { autoScroll = enable; }//设置曲线是否自动跟随
	bool isMiddleMove() { return middleMove; }//鼠标中键移动
	void setXAxisRange(double range);//设置X轴的显示范围
	void setTraceGraph(int index);//设置游标跟随的数据下标
	void modifyCurves();//改动显示内容函数
	void addData(SensorCache cache);//对某条曲线增加数据

	void setDiffSolveEnable(bool enable);//是否开启差值计算
	void setTracerEnable(bool enable);//是否使能游标
	void setScatterPointEnable(bool enable);//是否使能散点
	QVector<GraphData> *getGraphRecord() { return &_graphRecord; }	//获取历史数据指针
	double getBasicKey() { return basicKey; }

	bool reloadKey;//是否需要重置cntr(测试)
	double stopBasicKey;//停止显示时x轴变量的值(测试)

	#define DEFAULT_RANGE 20000

public slots:
	void storeAxisScope();//保存轴的范围
	void resumeAxisScope();//恢复轴的范围
	void showAllGraph();//显示所有
	void clearAllData();//清楚所有显示

private slots:
	void when_selectionChangedByUser();
	void when_legendDoubleClick(QCPLegend *legend, QCPAbstractLegendItem *item, QMouseEvent *event);

signals:
	void noniusValueChanged(int _curveIdx, QString val);//游标值变化了
	void autoRangeAdjusted(QCPRange range);//自动调整显示范围

protected:
	void timerEvent(QTimerEvent *event) Q_DECL_OVERRIDE;
	virtual void mousePressEvent(QMouseEvent *event) Q_DECL_OVERRIDE;
	virtual void mouseMoveEvent(QMouseEvent *event) Q_DECL_OVERRIDE;
	virtual void mouseReleaseEvent(QMouseEvent *event) Q_DECL_OVERRIDE;
	virtual void keyPressEvent(QKeyEvent *e) { if (e->key() == Qt::Key_Escape) { cancelRb = true; rb->hide(); } }

private:
	void addNewPointCache(GraphData *graphta, double key, double value);

private:
	bool displayUpdate;
	bool middleMove;

	double basicKey;//公用的x轴变量（测试）

	uint16_t lastCntr;//上一个cntr

	double xAxisRange;//x轴的最大显示范围
	double yMin;
	double yMax;
	void setXAxisDynamic(double range, double x);
	QVector<GraphData> _graphRecord;//所有曲线的数据,_graphRecord[n]中又含有两个QVector，也即第n条曲线的x、y值

	CurveCache *_cache;

	/*数据记录*/
	QMap<int, QCPGraph*> curveIdxTographPtr;//记录或查询：从曲线索引(key)->graph指针(val)。读之前必须查询key的存在性contains(key)?
	QMap<QCPGraph*, int> graphPtrTocurveIdx;//记录或查询：从graph指针(key)->曲线索引(val)。读之前必须查询key的存在性contains(key)?
	bool autoScroll;

	/*单击提示组件*/
	QCPItemText *presLabel;//单击时提示信息框
	QCPItemLine *presArrow;//提示信息的箭头
	/*右键菜单组件*/
	QAction *actClearDatas;//右键菜单:清空历史数据
	QAction *actshowAllGraph;//右键菜单:全显
	/*游标相关组件*/
	AxisTag *axisTracer;//坐标轴游标
	int traceIndex;//游标要吸附的数据源的下标
	bool tracerValid;//当前游标是否在图层上有效
	QCPGraph *traceGraph;//游标要吸附的graph
	int anchoringX;//鼠标铆钉的坐标

	bool tracerEnable;//是否使能游标
	/*两点求差组件*/
	bool diffSolveEnable;//是否使能两点求差
	QCPItemText *diffText;//单击时提示信息框
	QPointF *lastPoint;
	void solveDifference(QPointF newPoint);
	//放大功能
	QRubberBand *rb;
	QPoint startPos;
	bool cancelRb;
	//范围控制
	QVector<QCPRange> xRangeList;
	QVector<QCPRange> yRangeList;
};

