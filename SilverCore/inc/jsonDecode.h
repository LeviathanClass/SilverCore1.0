﻿#pragma once

#include <dirTree.h>
#include <QStringList>
#include <jsonConversion.h>
#include <jsonDirContent.h>

class JsonDecode {
public:
	JsonDecode();
	~JsonDecode();
	//获取对应tree的完整路径key数组
	static QList<QString> getCompleteDirKey(Tree T);
	//获取对应tree的完整路径json设置值的指令数组
	static QByteArray getCompleteDir(Tree T, QJsonValue value = 0);
	//获取对应tree的完整路径json取值的指令数组
	static QByteArray getSomewhereDirData(Tree T) {
		return getCompleteDir(T, "get");
	}
	//在tree目录下解析字符串
	static void decodeString(QString str, Tree jsonTree);
	//泛型取Tree的值
	template<typename T>
	static T *getTreeValue(Tree tree) {
		JsonDirContent *jsonDir = (JsonDirContent *)getTreeData(tree);
		T *value = (T *)jsonDir->getValue();
		return value;
	}
	//获取Tree的值类型
	static QJsonValue::Type getTreeType(Tree tree);
	//获取Tree内的key
	static QString getTreeKey(Tree T);
	//设置Tree内的dir
	static void setTreeDir(Tree T, const QString dir);
	//通过key找到Tree子成员
	static Tree findTreeChildByKey(Tree parent, const QString key);
	//根据路径在root目录下获取路径的tree节点，如路径不存在则返回空
	static Tree getTreeNodeByPaths(Tree root, const QString path);
	//在一个root目录中，根据路径添加jsonContent内容，如路径不存在会自动创建相关tree目录
	static void appendJsonDir(Tree root, QString path, JsonDirContent *content);
	//在一个root目录中，根据路径添加树内容，如路径不存在会自动创建目录
	static void appendJsonDir(Tree root, QString path, Tree contentTree);
private:
	//获取对应tree的值
	static QJsonValue getJsonValue(Tree T);
	//在tree目录下解析jsonValue，这是一个递归函数
	static void jsonValueUnpack(QJsonValue jsonValue, Tree T);
	//在tree目录下解析json，这是一个递归函数
	static void jsonTreeUnpack(QJsonObject json, Tree T);
	//为一个树的所有子目录添加dir信息，这是一个递归函数
	static void addDirToTree(const QString path, Tree contentTree);
	//为一个树更新dir信息
	static void jsonDirUpdate(Tree tree);
};
