﻿#pragma once

#include <QtWidgets/QTablewidget>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QLabel>
#include <QSettings>
#include "QEditableLable.h"

#if defined(_MSC_VER) && (_MSC_VER >= 1600)
# pragma execution_character_set("utf-8")
#endif

class SettingTable :
	public QTableWidget {
	Q_OBJECT
public:
	SettingTable(QWidget *parent);
	~SettingTable();

	void initState(QSettings *settings);

	QStringList header;

	//checkbox及可编辑label
	QWidget *editableWidget;
	QCheckBox *checkBox;
	QEditableLable *editableLable;
	QHBoxLayout *editableLayout;

	//接收到的位数
	QWidget *bitWidget;
	QLabel *bitLabel;
	QHBoxLayout *bitLayout;

	//显示的数值
	QWidget *numberWidget;
	QLineEdit *numberLineEdit;
	QHBoxLayout *numberLayout;

	void setItemCnt(int cnt) {
		itemCnt = cnt;
	}

private:
	int itemCnt;
};

