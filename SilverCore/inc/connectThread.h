﻿#pragma once
#include <QtCore/QThread>
#include <QtCore/QMutex>
#include <QtSerialPort/QSerialPort>
#include "circularArray.h"
#include "broadcastDecode.h"

#if defined(_MSC_VER) && (_MSC_VER >= 1600)
# pragma execution_character_set("utf-8")
#endif

class ConnectThread :
	public QThread {
	Q_OBJECT
public:
	ConnectThread();
	~ConnectThread();
	void stop();
	void openPort(QString portName, qint32 baudrate);
	void appendTx(QByteArray *src);
	uint64_t getRxLength() {
		return rxLength;
	}
	void setRxLength(uint64_t num) {
		rxLength = num;
	}
	uint64_t getTxLength() {
		return txLength;
	}
	void setTxLength(uint64_t num) {
		txLength = num;
	}
	BroadcastDecode *getDecode() {
		return broadcastDecode;
	}
	QString getPortName() {
		return _portName;
	}
	qint32 getBaudrate() {
		return _baudrate;
	}
signals:
	void error(QString tips);
	void displayTxNum(const uint64_t &a);
	void displayRxNum(const uint64_t &a);
	void rawStringDecode(QByteArray);
private:
	void run();
	bool stoped;
	QMutex sendMutex;
	QByteArray sendData;
	CircularArray *circularArray;
	BroadcastDecode *broadcastDecode;
	QString _portName;
	qint32 _baudrate;
	QSerialPort *serialPort;
	uint64_t txLength;
	uint64_t rxLength;
};

