﻿#pragma once
#include <QtWidgets/QWidget>
#include <ui_dataWidgetUi.h>
#include <QtCore/QTimer>
#include <QtSerialPort/QSerialPort>
#include <QtSerialPort/QSerialPortInfo>
#include <QtAwesome.h>
#include "displayThread.h"

#if defined(_MSC_VER) && (_MSC_VER >= 1600)
# pragma execution_character_set("utf-8")
#endif

class DataWidget :
	public QWidget {
	Q_OBJECT
public:
	DataWidget();
	~DataWidget();
	DisplayThread *getDisplayThread() {
		return displayThread;
	}

signals:
	void operationRecord(fa::icon icon, QString record);

private slots:
	void displayRxText(QString);
	void displayTxText(QString);
	void txDisplayNum(uint64_t length);
	void rxDisplayNum(uint64_t);
	void displayClicked();
	void clearTx();
	void clearRx();

private:
	Ui::dataWidgetUi ui;
	DisplayThread *displayThread;
};

