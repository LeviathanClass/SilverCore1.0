﻿#pragma once
#include <QtWidgets/QWidget>
#include <ui_calibrateWidgetUi.h>
#include "config.h"
#include "connectThread.h"
#include <QtAwesome.h>

#if defined(_MSC_VER) && (_MSC_VER >= 1600)
# pragma execution_character_set("utf-8")
#endif

class CalibrateWidget :
	public QWidget {
	Q_OBJECT
public:
	CalibrateWidget(ConnectThread *thread, Config *config);
	~CalibrateWidget();
protected:
	void timerEvent(QTimerEvent *event);
signals:
	void operationRecord(fa::icon icon, QString record);
private slots:
	void gyroAllanProcessDecode(JsonDirContent content);
	void accelHexahedronProcessDecode(JsonDirContent content);
	void setAllControlEnable(bool enabled);
	void calibrationDirective();
	void selectedTips(int selected);
private:
	void createJsonControlMapping();
	void setPortControlVisible(bool visible);
	void updateHexahedronPicture(uint32_t number);
private:
	Ui::calibrateWidgetUi ui;

	QString _caliState;
	Config *_config;
	ConnectThread *_connectThread;
	uint32_t _caliProgress;	//校准计数
};
