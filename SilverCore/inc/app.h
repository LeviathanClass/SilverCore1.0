﻿#pragma once
#include <QtCore/QStringList>
#include <QtCore/QMutex>
#include <QtCore/QVector>

#if defined(_MSC_VER) && (_MSC_VER >= 1600)
# pragma execution_character_set("utf-8")
#endif

class SensorCache {
public:
	SensorCache(uint8_t index = 0U, uint8_t length = 0U, uint8_t properties = 0U, uint16_t cntr = 0U, double value = 0.0) {
		_curvesIndex = index;
		_dataLength = length;
		_properties = properties;
		_cntr = cntr;
		_value = value;
	}
	void setData(uint8_t index, uint8_t length, uint8_t properties, uint16_t cntr, double value) {
		_curvesIndex = index;
		_dataLength = length;
		_properties = properties;
		_cntr = cntr;
		_value = value;
	}
	uint8_t getIndex() {
		return _curvesIndex;
	}
	uint8_t getLength() {
		return _dataLength;
	}
	uint8_t getProperties() {
		return _properties;
	}
	uint16_t getCntr() {
		return _cntr;
	}
	double getValue() {
		return _value;
	}
private:
	//当前曲线的下标
	uint8_t _curvesIndex;
	//当前曲线的长度
	uint8_t _dataLength;
	//当前曲线数据的属性
	uint8_t _properties;
	//当前曲线数据的时间戳（16bit）
	uint16_t _cntr;
	//当前曲线的数据
	double _value;
};

class App {
public:
	App();
	static QStringList rxStringList;
	static QStringList txStringList;
	static QMutex textMutex;
	static QVector<SensorCache> sensorCurveCache;
	static QMutex curvesMutex;
};
