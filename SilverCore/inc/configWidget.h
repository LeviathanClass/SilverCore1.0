﻿#pragma once
#include <QtWidgets/QWidget>
#include <QtCore/QMap>
#include <QSettings>
#include <ui_configWidgetUi.h>
#include "connectThread.h"
#include "config.h"
#include <QtAwesome.h>
#include <jsonConversion.h>
#include <jsonDirContent.h>
#include <jsonDecode.h>

#if defined(_MSC_VER) && (_MSC_VER >= 1600)
# pragma execution_character_set("utf-8")
#endif

class ConfigWidget :
	public QWidget {
	Q_OBJECT
public:
	ConfigWidget(ConnectThread *thread, Config *config, QSettings *settings);
	~ConfigWidget();
signals:
	void operationRecord(fa::icon icon, QString record);
	void reConnectPort(bool enable, bool init);
private slots:
	void setGetConfigEnable(bool enable);
	void setAllControlEnable(bool enable);
	void configurationDirective();
	void configStringDecode(QString str);
protected:
	void timerEvent(QTimerEvent *event) Q_DECL_OVERRIDE;

private:
	void createJsonControlMapping();

private:
	Ui::configWidgetUi ui;
	bool configControlLastEnabled;

	ConnectThread *_connectThread;
	Config *_config;
	//config
	QSettings *_settings;
	QString _reconnectPortName;
	qint32 _reconnectBaudrate;
	int _reconnectTimerId;
};

