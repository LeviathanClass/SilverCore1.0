﻿#include "axistag.h"

AxisTag::AxisTag(QCPAxis *xAxis, QCPAxis *yAxis) :
	_xAxis(xAxis),
	_yAxis(yAxis) {
	tracer = new QCPItemTracer(xAxis->parentPlot());
	tracer->setVisible(false);
	tracer->setPen(QPen(Qt::DashLine));//虚线游标

	yArrow = new QCPItemLine(yAxis->parentPlot());
	yArrow->setLayer("overlay");
	yArrow->setClipToAxisRect(false);
	yArrow->setHead(QCPLineEnding::esSpikeArrow);
	yArrow->end->setParentAnchorY(tracer->position);

	yArrow->end->setTypeX(QCPItemPosition::ptAxisRectRatio);
	yArrow->end->setCoords(1, 0);
	yArrow->start->setParentAnchor(yArrow->end);
	yArrow->start->setCoords(15, 0);
  
	yLabel = new QCPItemText(yAxis->parentPlot());
	yLabel->setLayer("overlay");
	yLabel->setClipToAxisRect(false);
	yLabel->setPadding(QMargins(3, 0, 3, 0));
	yLabel->setBrush(QBrush(Qt::white));
	yLabel->setPen(QPen(Qt::blue));
	yLabel->setColor(Qt::black);
	yLabel->setFont(QFont("Microsoft YaHei", 6, QFont::Normal));
	yLabel->setPositionAlignment(Qt::AlignLeft|Qt::AlignVCenter);
	yLabel->position->setParentAnchor(yArrow->start);

	xArrow = new QCPItemLine(xAxis->parentPlot());
	xArrow->setLayer("overlay");
	xArrow->setClipToAxisRect(false);
	xArrow->setHead(QCPLineEnding::esSpikeArrow);
	xArrow->end->setParentAnchorX(tracer->position);

	xArrow->end->setTypeY(QCPItemPosition::ptAxisRectRatio);
	xArrow->end->setCoords(0, 1);
	xArrow->start->setParentAnchor(xArrow->end);
	xArrow->start->setCoords(0, 15);

	xLabel = new QCPItemText(xAxis->parentPlot());
	xLabel->setLayer("overlay");
	xLabel->setClipToAxisRect(false);
	xLabel->setPadding(QMargins(3, 0, 3, 0));
	xLabel->setBrush(QBrush(Qt::white));
	xLabel->setPen(QPen(Qt::blue));
	xLabel->setColor(Qt::black);
	xLabel->setFont(QFont("Microsoft YaHei", 6, QFont::Normal));
	xLabel->setPositionAlignment(Qt::AlignHCenter | Qt::AlignTop);
	xLabel->position->setParentAnchor(xArrow->start);

	this->setText("none...", "none...");
	this->setVisible(false);
}

AxisTag::~AxisTag() {
	if (tracer)
		tracer->parentPlot()->removeItem(tracer);
	if (xArrow)
	xArrow->parentPlot()->removeItem(xArrow);
	if (xLabel)
	xLabel->parentPlot()->removeItem(xLabel);
	if (yArrow)
		yArrow->parentPlot()->removeItem(yArrow);
	if (yLabel)
		yLabel->parentPlot()->removeItem(yLabel);
}

void AxisTag::setPen(const QPen &pen) {
	xArrow->setPen(pen);
	xLabel->setPen(pen);
	yArrow->setPen(pen);
	yLabel->setPen(pen);
}

void AxisTag::setColor(const QColor &color) {
	xLabel->setColor(color);
	yLabel->setColor(color);
}

void AxisTag::setBrush(const QBrush &brush) {
  xLabel->setBrush(brush);
  yLabel->setBrush(brush);
}

void AxisTag::setText(const QString &xText, const QString &yText) {
  xLabel->setText(xText);
  yLabel->setText(yText);
}

void AxisTag::setVisible(bool visible) {
	tracer->setVisible(visible);
	xArrow->setVisible(visible);
	xLabel->setVisible(visible);
	yArrow->setVisible(visible);
	yLabel->setVisible(visible);
}

void AxisTag::setGraph(QCPGraph *graph) {
	tracer->setGraph(graph);
}

void AxisTag::updatePosition(QCPGraph *graph, double x) {
	tracer->setGraph(graph);
	QColor color = graph->pen().color();
	QPen pen = QPen(Qt::DashLine);
	pen.setColor(color);
	tracer->setPen(pen);
	tracer->setGraphKey(x);
	float traceX = tracer->position->key();
	float traceY = tracer->position->value();
	//同步颜色
	this->setPen(graph->pen());
	this->setBrush(QBrush(color));
	//更新显示内容
	this->setText(QString("x : \r\n") + QString::number(traceX, 'f', 3), QString("y :\r\n") + QString::number(traceY, 'f', 3));
}
