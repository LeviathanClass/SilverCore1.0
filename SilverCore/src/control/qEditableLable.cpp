﻿#include "qEditableLable.h"
#include <QtWidgets/QColorDialog>

QEditableLable::QEditableLable(QWidget *parent, QColor color, QString name) :
	curveColor(color),
	lastColor(color),
	curveName(name),
	lastName(curveName) {

	Q_UNUSED(*parent)
	this->setObjectName(QString::fromUtf8("stackedWidget"));
	this->setMinimumSize(QSize(0, 20));

	page = new QWidget();
	page->setObjectName(QString::fromUtf8("page"));
	horizontalLayout = new QHBoxLayout(page);
	horizontalLayout->setSpacing(0);
	horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
	horizontalLayout->setContentsMargins(1, 1, 1, 1);
	label = new QLabel(page);
	label->setObjectName(QString::fromUtf8("label"));
	QPalette initPalette;
	initPalette.setColor(QPalette::Text, curveColor);
	label->setPalette(initPalette);
	label->setAlignment(Qt::AlignLeading | Qt::AlignLeft | Qt::AlignVCenter);
	label->setFont(QFont("Microsoft YaHei", 8, QFont::Bold));
	label->setText(curveName);
	horizontalLayout->addWidget(label);
	this->addWidget(page);

	page_1 = new QWidget();
	page_1->setObjectName(QString::fromUtf8("page_1"));
	horizontalLayout_1 = new QHBoxLayout(page_1);
	horizontalLayout_1->setSpacing(0);
	horizontalLayout_1->setContentsMargins(1, 1, 1, 1);
	horizontalLayout_1->setObjectName(QString::fromUtf8("horizontalLayout_1"));
	lineEdit = new QLineEdit(page_1);
	lineEdit->setObjectName(QString::fromUtf8("lineEdit"));
	lineEdit->setMinimumSize(QSize(0, 20));
	lineEdit->setAlignment(Qt::AlignLeading | Qt::AlignLeft | Qt::AlignVCenter);
	lineEdit->setFont(QFont("Microsoft YaHei", 8, QFont::Normal));
	horizontalLayout_1->addWidget(lineEdit);
	this->addWidget(page_1);
	this->setCurrentIndex(0);

	//右键菜单
	this->setContextMenuPolicy(Qt::ActionsContextMenu);
	//菜单-按钮1
	actColor = new QAction(tr("修改颜色"));
	connect(actColor, &QAction::triggered, [this]() {
		QPalette pal = label->palette();  //获取现有颜色
		QColor inintColor = pal.color(QPalette::Text); //现有文字颜色
		QColor _color = QColorDialog::getColor(inintColor, this, "选择颜色");
		if (!_color.isValid()) {
			pal.setColor(QPalette::Text, inintColor);
		}
		else {
			curveColor = _color;	//备份
			pal.setColor(QPalette::Text, _color);
			if (lastColor != _color) {
				label->setPalette(pal);	//设置字体颜色
				emit colorChanged(curveColor);	//颜色变更信号
			}
			lastColor = _color;
		}
	});
	this->addAction(actColor);
	//菜单-按钮2
	actName = new QAction(tr("修改名称"));
	connect(actName, SIGNAL(triggered(bool)), this, SLOT(modifyName()));
	this->addAction(actName);

	label->installEventFilter(this);
	lineEdit->installEventFilter(this);
}

QEditableLable::~QEditableLable()
{

}

bool QEditableLable::eventFilter(QObject* obj, QEvent* evt) {
	if (obj == label) {
		if (evt->type() == QEvent::MouseButtonDblClick) {
			lineEdit->setText(label->text());
			this->setCurrentIndex(1);
			lineEdit->setFocus();
		}
	}
	else if (obj == lineEdit) {
		if (evt->type() == QEvent::KeyPress) {
			QKeyEvent* keyevt = static_cast<QKeyEvent*>(evt);
			// Qt::Key_Return是大键盘的回车键 Qt::Key_Enter是小键盘的回车键
			if ((keyevt->key() == Qt::Key_Return) ||
				(keyevt->key() == Qt::Key_Escape) ||
				(keyevt->key() == Qt::Key_Enter)) {
				label->setText(lineEdit->text());
				this->setCurrentIndex(0);
				if (lastName != lineEdit->text()) {
					curveName = lineEdit->text();
					emit nameChanged(curveName);
				}
				lastName = lineEdit->text();
			}
		}
		else if (evt->type() == QEvent::FocusOut) {
			label->setText(lineEdit->text());
			this->setCurrentIndex(0);
			if (lastName != lineEdit->text()) {
				curveName = lineEdit->text();
				emit nameChanged(curveName);
			}
			lastName = lineEdit->text();
		}
	}
	return QWidget::eventFilter(obj, evt);
}

void QEditableLable::modifyName() {
	lineEdit->setText(label->text());
	this->setCurrentIndex(1);
	lineEdit->setFocus();
}

void QEditableLable::setText(QString name) {
	label->setText(name);
	curveName = name;
	emit nameChanged(curveName);
}

void QEditableLable::setColor(QColor color) {
	QPalette pal = label->palette();
	pal.setColor(QPalette::Text, color);
	curveColor = color;
	if (lastColor != curveColor) {
		label->setPalette(pal);	//设置字体颜色
		emit colorChanged(curveColor);	//颜色变更信号
	}
	lastColor = curveColor;
}
