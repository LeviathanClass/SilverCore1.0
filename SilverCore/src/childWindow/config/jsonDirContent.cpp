﻿#include <jsonDirContent.h>

JsonDirContent::JsonDirContent(const Tree T) {
	JsonDirContent *content = (JsonDirContent *)T->data;
	_key = content->getKey();
	_value = content->getValue();
	_type = content->getType();
	_dir = content->getDir();
}

JsonDirContent::JsonDirContent(const JsonDirContent &content) {
	_key = content._key;
	_value = content._value;
	_type = content._type;
	_control = content._control;
	_dir = content._dir;
}

JsonDirContent::JsonDirContent(const QString key, const void * value, const QJsonValue::Type type, const QWidget * control, const ConnectType connect, const QString dir) {
	_key = key;
	_value = (void *)value;
	_type = type;
	_control = (QWidget *)control;
	_connect = connect;
	_dir = dir;
}

JsonDirContent::~JsonDirContent() {

}

