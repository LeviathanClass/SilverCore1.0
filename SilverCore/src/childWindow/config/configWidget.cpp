﻿#include <configWidget.h>
#include <QMessageBox>
#include <QtCore/QDebug>
#include <app.h>


//构造函数
ConfigWidget::ConfigWidget(ConnectThread *thread, Config *config, QSettings *settings):
	_config(config),
	_connectThread(thread),
	_settings(settings), 
	configControlLastEnabled(false) {
	//加上这一句才会自动释放对象
	this->setAttribute(Qt::WA_DeleteOnClose);
	ui.setupUi(this);

	//初始化QtAwesome
	QtAwesome* awesome = new QtAwesome();
	awesome->initFontAwesome();
	QVariantMap options;
	//初始化颜色
	options.insert("color", QColor(0x94ffff));
	options.insert("color-active", QColor(0x94ffff));
	//按键添加图标
	ui.softResetBtn->setIcon(awesome->icon(fa::refresh, options));
	ui.restoreFactoryBtn->setIcon(awesome->icon(fa::building, options));
	ui.getConfigBtn->setIcon(awesome->icon(fa::search, options));
	ui.saveConfigBtn->setIcon(awesome->icon(fa::save, options));

	//全部控件失能，等待设备连接
	setGetConfigEnable(false);
	QObject::connect(_connectThread->getDecode(), SIGNAL(stringDecode(QString)), this, SLOT(configStringDecode(QString)));
	
	//建立json到控件的映射
	createJsonControlMapping();

	//测试中，将加载配置的功能失能
	ui.loadConfigFileBtn->setEnabled(false);
	ui.saveConfigFileBtn->setEnabled(false);
}

void ConfigWidget::createJsonControlMapping() {
	//接口设置页
	_config->setJsonDirControl("/interface/baudrate", ui.uartBaudRateComboBox, JsonDirContent::allResponse);
	_config->setJsonDirControl("/interface/broadcastID", ui.broadcastIDIntegerSpin, JsonDirContent::allResponse);
	_config->setJsonDirControl("/interface/settingID", ui.settingIDIntegerSpin, JsonDirContent::allResponse);
	//安装信息叶
	_config->setJsonDirControl("/install/yBias", ui.yBiasDoubleSpin, JsonDirContent::allResponse);
	_config->setJsonDirControl("/install/xBias", ui.xBiasDoubleSpin, JsonDirContent::allResponse);
	_config->setJsonDirControl("/install/zBias", ui.zBiasDoubleSpin, JsonDirContent::allResponse);
	//报文——开关信息叶
	_config->setJsonDirControl("/msg/switch/uartMsg", ui.usartBroadcastSwitch, JsonDirContent::allResponse);
	_config->setJsonDirControl("/msg/switch/canMsg", ui.canBroadcastSwitch, JsonDirContent::allResponse);
	_config->setJsonDirControl("/msg/switch/usbMsg", ui.usbBroadcastSwitch, JsonDirContent::allResponse);
	_config->setJsonDirControl("/msg/switch/gyroMsg", ui.gyroOutputSwitch, JsonDirContent::allResponse);
	_config->setJsonDirControl("/msg/switch/accelMsg", ui.accelOutputSwitch, JsonDirContent::allResponse);
	_config->setJsonDirControl("/msg/switch/magMsg", ui.magOutputSwitch, JsonDirContent::allResponse);
	_config->setJsonDirControl("/msg/switch/presMsg", ui.presOutputSwitch, JsonDirContent::allResponse);
	_config->setJsonDirControl("/msg/switch/attitudeMsg", ui.attitudeOutputSwitch, JsonDirContent::allResponse);
	//报文——转发开关叶
	_config->setJsonDirControl("/msg/trans/usb_Can", ui.usbForwardCanSwitch, JsonDirContent::allResponse);
	_config->setJsonDirControl("/msg/trans/usb_Uart", ui.usbForwardUartSwitch, JsonDirContent::allResponse);
	_config->setJsonDirControl("/msg/trans/uart_Can", ui.uartForwardCanSwitch, JsonDirContent::allResponse);
	//报文——开关信息叶
	_config->setJsonDirControl("/msg/format/accelFusion", ui.accelFusionSwitch, JsonDirContent::allResponse);
	_config->setJsonDirControl("/msg/format/magFusion", ui.magFusionSwitch, JsonDirContent::allResponse);
	_config->setJsonDirControl("/msg/format/yawContinus", ui.yawContinuousSwitch, JsonDirContent::allResponse);
	_config->setJsonDirControl("/msg/format/gyroType", ui.gyroTypeSwitch, JsonDirContent::allResponse);
	_config->setJsonDirControl("/msg/format/accelType", ui.accelTypeSwitch, JsonDirContent::allResponse);
	_config->setJsonDirControl("/msg/format/attitudeType", ui.attitudeTypeSwitch, JsonDirContent::allResponse);
	_config->setJsonDirControl("/msg/format/heightType", ui.heightTypeSwitch, JsonDirContent::allResponse);
	_config->setJsonDirControl("/msg/format/gyroLength", ui.gyroLengthSwitch, JsonDirContent::allResponse);
	_config->setJsonDirControl("/msg/format/accelLength", ui.accelLengthSwitch, JsonDirContent::allResponse);
	_config->setJsonDirControl("/msg/format/magLength", ui.magLengthSwitch, JsonDirContent::allResponse);
	_config->setJsonDirControl("/msg/format/attitudeLength", ui.attitudeLengthSwitch, JsonDirContent::allResponse);
	//传感器设置叶
	_config->setJsonDirControl("/sensor/imuType", ui.imuTypeComboBox, JsonDirContent::allResponse);
	_config->setJsonDirControl("/sensor/gyroODR", ui.gyroODRComboBox, JsonDirContent::allResponse);
	_config->setJsonDirControl("/sensor/accelODR", ui.accelODRComboBox, JsonDirContent::allResponse);
	_config->setJsonDirControl("/sensor/attitudeODR", ui.attitudeODRComboBox, JsonDirContent::allResponse);
	_config->setJsonDirControl("/sensor/magODR", ui.magODRComboBox, JsonDirContent::allResponse);
	_config->setJsonDirControl("/sensor/heightODR", ui.heightODRComboBox, JsonDirContent::allResponse);
	_config->setJsonDirControl("/sensor/gyroFSR", ui.gyroFSRComboBox, JsonDirContent::allResponse);
	_config->setJsonDirControl("/sensor/accelFSR", ui.accelFSRComboBox, JsonDirContent::allResponse);
	//配置组
	_config->setJsonDirControl("/setGroup", ui.configGroupComboBox, JsonDirContent::allResponse);
}

//析构函数
ConfigWidget::~ConfigWidget() {
	
}

//slot：获取设备信息使能设置，必须获取设备信息成功后才会使能其他设置类控件
void ConfigWidget::setGetConfigEnable(bool enable) {
	ui.getConfigBtn->setEnabled(enable);
	setAllControlEnable(enable);
	//当获取设备信息失能时，其余控件会一起被失能
	if (!enable) {
		setAllControlEnable(enable);
	}
	//如果被使能，则自动获取一次配置信息
	else {
		//获取配置组
		_config->getJsonDirValue("/setGroup");
		//获取配置信息
		_config->getJsonDirValue("/interface/");
		_config->getJsonDirValue("/install/");
		_config->getJsonDirValue("/msg/");
		_config->getJsonDirValue("/sensor/");
		emit operationRecord(fa::search, tr("获取模块配置信息"));
	}
}

//slot：总控件的使能设置
void ConfigWidget::setAllControlEnable(bool enable) {
	//if ((!configControlLastEnabled && enable) || !enable) {
		ui.configGroupComboBox->setEnabled(enable);
		ui.saveConfigBtn->setEnabled(enable);
		ui.restoreFactoryBtn->setEnabled(enable);
		ui.softResetBtn->setEnabled(enable);
	//}
	configControlLastEnabled = enable;
}

void ConfigWidget::configurationDirective() {
	QPushButton *btn = (QPushButton *)sender();
	//必须打开串口
	if (_connectThread->isRunning()) {
		if (btn->objectName() == ui.getConfigBtn->objectName()) {
			//获取配置组
			_config->getJsonDirValue("/setGroup");
			//获取配置信息
			_config->getJsonDirValue("/interface/");
			_config->getJsonDirValue("/install/");
			_config->getJsonDirValue("/msg/");
			_config->getJsonDirValue("/sensor/");
			emit operationRecord(fa::search, tr("获取模块配置信息"));
		}
		else if (btn->objectName() == ui.saveConfigBtn->objectName()) {
			if (QMessageBox::No == QMessageBox::information(this,
				"再次确认",
				"确认后将保存当前配置",
				QMessageBox::Yes,
				QMessageBox::No | QMessageBox::Default)) {
				return;
			}
			bool boolean = true;
			_config->sendJsonDirValue("/system/save", &boolean);
			emit operationRecord(fa::save, tr("保存模块配置"));
		}
		else if (btn->objectName() == ui.restoreFactoryBtn->objectName()) {
			if (QMessageBox::No == QMessageBox::information(this,
				"再次确认",
				"确认后将所有配置信息恢复到出厂设置",
				QMessageBox::Yes,
				QMessageBox::No | QMessageBox::Default)) {
				return;
			}
			//发送恢复出厂json
			bool boolean = true;
			_config->sendJsonDirValue("/system/restore", &boolean);
			emit operationRecord(fa::building, tr("恢复出厂设置"));
		}
		else if (btn->objectName() == ui.softResetBtn->objectName()) {
			//发送复位json
			bool boolean = true;
			_config->sendJsonDirValue("/system/reset", &boolean);
			//50ms后自动断开，并触发自动重连
			_reconnectTimerId = this->startTimer(50);
			emit operationRecord(fa::refresh, tr("软件复位"));
		}
		else if (btn->objectName() == ui.loadConfigFileBtn->objectName()) {
			emit operationRecord(fa::upload, tr("加载配置表"));
		}
		else if (btn->objectName() == ui.saveConfigFileBtn->objectName()) {
			
		}
	}
	else {
		//否则询问是否打开串口

	}
}

void ConfigWidget::timerEvent(QTimerEvent *event) {
	//boot --> app跳转重连
	if (event->timerId() == _reconnectTimerId) {
		//自动发送重连定时器相应
		if (!_connectThread->isRunning()) {
			_connectThread->openPort(_reconnectPortName, _reconnectBaudrate);
			emit reConnectPort(true, true);
			this->killTimer(_reconnectTimerId);
			qDebug() << "auto reconnect!";
			_reconnectTimerId = 0;
			operationRecord(fa::refresh, tr("复位重连成功！"));
		}
		else {
			//打开自动重连
			_reconnectPortName = _connectThread->getPortName();
			_reconnectBaudrate = _connectThread->getBaudrate();
			//关闭串口
			_connectThread->stop();
			emit reConnectPort(false, true);
			this->killTimer(_reconnectTimerId);
			//800ms后自动重连
			_reconnectTimerId = this->startTimer(800);
		}
	}
}

//有关配置信息的log解码函数
void ConfigWidget::configStringDecode(QString str) {
	QRegExp saveExp("<system> Saved successfully.");
	if (str.indexOf(saveExp) >= 0) {
		emit operationRecord(fa::check, tr("配置保存成功"));
	}
	QRegExp factoryExp("<system> Factory settings restored successfully.");
	if (str.indexOf(factoryExp) >= 0) {
		emit operationRecord(fa::check, tr("恢复出厂设置成功"));
	}
}

