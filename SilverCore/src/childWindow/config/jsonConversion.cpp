﻿#include <jsonConversion.h>
#include <qDebug>

//QString >> QJsonObject
QJsonObject JsonConversion::getJsonObjectFromString(const QString jsonString) {
	QJsonDocument jsonDocument = QJsonDocument::fromJson(jsonString.toLocal8Bit().data());
	return jsonDocument.object();
}
//QString >> QJsonArray
QJsonArray JsonConversion::getJsonArrayFromString(const QString jsonString) {
	QJsonDocument jsonDocument = QJsonDocument::fromJson(jsonString.toLocal8Bit().data());
	return jsonDocument.array();
}
//QString >> QByteArray
QByteArray JsonConversion::getByteArrayFromString(const QString& jsonString) {
	return jsonString.toLatin1();
}

//QJsonObject >> QByteArray
QByteArray JsonConversion::getByteArrayFromJsonObject(const QJsonObject& jsonObject) {
	return QJsonDocument(jsonObject).toJson(QJsonDocument::Compact);
}
//QJsonObject >> QString
QString JsonConversion::getStringFromJsonObject(const QJsonObject& jsonObject) {
	return QString(QJsonDocument(jsonObject).toJson());
}

//QJsonArray >> QByteArray
QByteArray JsonConversion::getByteArrayFromJsonArray(const QJsonArray& jsonArray) {
	return QJsonDocument(jsonArray).toJson();
}
//QJsonArray >> QString
QString JsonConversion::getStringFromJsonArray(const QJsonArray& jsonArray) {
	return QString(QJsonDocument(jsonArray).toJson());
}

//QByteArray >> QJsonArray
QJsonArray JsonConversion::getJsonArrayFromByteArray(const QByteArray& jsonByte) {
	return QJsonDocument::fromJson(jsonByte).array();
}
//QByteArray >> QJsonObject
QJsonObject JsonConversion::getJsonObjectFromByteArray(const QByteArray& jsonByte) {
	return QJsonDocument::fromJson(jsonByte).object();
}
//QByteArray >> QString
QString JsonConversion::getStringFromByteArray(const QByteArray& jsonByte) {
	return QString(jsonByte);;
}