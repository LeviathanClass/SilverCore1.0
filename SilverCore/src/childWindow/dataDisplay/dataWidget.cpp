﻿#include <dataWidget.h>
#include <QtCore/QDateTime>
#include <QtCore/QDebug>
#include <QtCore/QEvent>
#include <plotWidget.h>
#include <app.h>

//构造函数
DataWidget::DataWidget() {
	//加上这一句才会自动释放对象
	this->setAttribute(Qt::WA_DeleteOnClose);
	ui.setupUi(this);

	//初始化QtAwesome
	QtAwesome* awesome = new QtAwesome();
	awesome->initFontAwesome();
	QVariantMap options;
	//初始化颜色
	options.insert("color", QColor(0x94ffff));
	options.insert("color-active", QColor(0x94ffff));
	//按键添加图标
	ui.clearLogBtn->setIcon(awesome->icon(fa::eraser, options));
	ui.saveLogBtn->setIcon(awesome->icon(fa::save, options));
	ui.clearRxBtn->setIcon(awesome->icon(fa::eraser, options));
	ui.clearTxBtn->setIcon(awesome->icon(fa::eraser, options));
	options.insert("text-off", QString(fa::eye));
	ui.showRxBtn->setIcon(awesome->icon(fa::eyeslash, options));
	ui.showTxBtn->setIcon(awesome->icon(fa::eyeslash, options));

	displayThread = new DisplayThread;
	QObject::connect(displayThread, SIGNAL(displayRxText(QString)), this, SLOT(displayRxText(QString)));
	QObject::connect(displayThread, SIGNAL(displayTxText(QString)), this, SLOT(displayTxText(QString)));

	qRegisterMetaType<uint64_t>("uint64_t");
	QObject::connect(displayThread, SIGNAL(rxDisplayNum(uint64_t)), this, SLOT(rxDisplayNum(uint64_t)));
	QObject::connect(displayThread, SIGNAL(txDisplayNum(uint64_t)), this, SLOT(txDisplayNum(uint64_t)));
	//显示线程打开
	displayThread->start();
}

//析构函数
DataWidget::~DataWidget() {
	displayThread->stop();
}

//slot:显示发送数量
void DataWidget::txDisplayNum(uint64_t length) {
	ui.TxNumLabel->setText(QString::number(length));
}

//slot:显示接收数量
void DataWidget::rxDisplayNum(uint64_t length) {
	ui.RxNumLabel->setText(QString::number(length));
}

//slot:显示接收text
void DataWidget::displayRxText(const QString rx) {
	QString afterLine;
	//光标列数
	int cursorCol = ui.displayRxBrowser->textCursor().columnNumber();
	//每行最大显示的字符数
	int lineMaxWidth = ui.displayRxBrowser->lineWrapColumnOrWidth();
	//字符串的剩余长度
	int resLength = rx.length();

	//如果当前列数 + 输入长度 小于最大显示长度则直接添加
	if ((cursorCol + rx.length()) < lineMaxWidth) {
		//displayList.append(b);
		afterLine = rx;
	}
	//否则进行裁剪后再添加
	else {
		//直到所有值都被赋值完毕
		while (resLength > 0) {
			int cutLength;
			//如果剩余长度 + 光标长度 大于最大长度
			if (resLength + cursorCol > lineMaxWidth) {
				//则截取满足当前行的长度
				cutLength = lineMaxWidth - cursorCol;
			}
			//否则全部截取
			else {
				cutLength = resLength;
			}
			//更新下一次的光标并保证在最大长度以内
			if (cursorCol + cutLength >= lineMaxWidth) {
				cursorCol = cursorCol + cutLength - lineMaxWidth;
			}
			else {
				cursorCol += cutLength;
			}
			//对string进行截取并插入换行
			if (cursorCol != 0) {
				afterLine += rx.mid(rx.length() - resLength, cutLength);
			}
			else {
				afterLine += rx.mid(rx.length() - resLength, cutLength) + "\n";
			}
			//更新剩余长度
			resLength -= cutLength;
		}
	}
	//显示
	ui.displayRxBrowser->insertPlainText(afterLine);
	ui.displayRxBrowser->moveCursor(QTextCursor::End);
}

//slot:显示发送text
void DataWidget::displayTxText(const QString tx) {
	QString afterLine;
	//光标列数
	int cursorCol = ui.displayTxBrowser->textCursor().columnNumber();
	//每行最大显示的字符数
	int lineMaxWidth = ui.displayTxBrowser->lineWrapColumnOrWidth();
	//字符串的剩余长度
	int resLength = tx.length();

	//如果当前列数 + 输入长度 小于最大显示长度则直接添加
	if ((cursorCol + tx.length()) < lineMaxWidth) {
		//displayList.append(b);
		afterLine = tx;
	}
	//否则进行裁剪后再添加
	else {
		//直到所有值都被赋值完毕
		while (resLength > 0) {
			int cutLength;
			//如果剩余长度 + 光标长度 大于最大长度
			if (resLength + cursorCol > lineMaxWidth) {
				//则截取满足当前行的长度
				cutLength = lineMaxWidth - cursorCol;
			}
			//否则全部截取
			else {
				cutLength = resLength;
			}
			//更新下一次的光标并保证在最大长度以内
			if (cursorCol + cutLength >= lineMaxWidth) {
				cursorCol = cursorCol + cutLength - lineMaxWidth;
			}
			else {
				cursorCol += cutLength;
			}
			//对string进行截取并插入换行
			if (cursorCol != 0) {
				afterLine += tx.mid(tx.length() - resLength, cutLength);
			}
			else {
				afterLine += tx.mid(tx.length() - resLength, cutLength) + "\n";
			}
			//更新剩余长度
			resLength -= cutLength;
		}
	}
	//显示
	ui.displayTxBrowser->insertPlainText(afterLine);
	ui.displayTxBrowser->moveCursor(QTextCursor::End);
	////显示
	//ui.displayTxBrowser->append(tx);
}

//slot:显示开关
void DataWidget::displayClicked() {
	QPushButton *btn = (QPushButton *)sender();
	if (btn->isChecked()) {
		btn->setText(QObject::tr("停止显示"));
		QDateTime currentTime = QDateTime::currentDateTime();
		if (btn->objectName() == ui.showTxBtn->objectName()) {
			displayThread->setTxDisplay(true);
			ui.journalBrowser->append(currentTime.toString("[yyyy.MM.dd hh:mm:ss.zzz] ") + "Start Tx Show.");
			emit operationRecord(fa::eye, tr("开启数据发送显示"));
		}
		else {
			displayThread->setRxDisplay(true);
			ui.journalBrowser->append(currentTime.toString("[yyyy.MM.dd hh:mm:ss.zzz] ") + "Start Rx Show.");
			emit operationRecord(fa::eye, tr("开启数据接收显示"));
		}
	}
	else {
		btn->setText(QObject::tr("显示"));
		QDateTime currentTime = QDateTime::currentDateTime();
		if (btn->objectName() == ui.showTxBtn->objectName()) {
			displayThread->setTxDisplay(false);
			ui.journalBrowser->append(currentTime.toString("[yyyy.MM.dd hh:mm:ss.zzz] ") + "Stop Tx Show.");
			emit operationRecord(fa::eyeslash, tr("关闭数据发送显示"));
		}
		else {
			displayThread->setRxDisplay(false);
			ui.journalBrowser->append(currentTime.toString("[yyyy.MM.dd hh:mm:ss.zzz] ") + "Stop Rx Show.");
			emit operationRecord(fa::eyeslash, tr("关闭数据接收显示"));
		}
	}
}

//清除发送内容
void DataWidget::clearTx() {
	ui.displayTxBrowser->clear();
	displayThread->setTxLength(0);
	txDisplayNum(0);
	emit operationRecord(fa::eraser, tr("清除数据发送内容"));
}

//清除接收内容
void DataWidget::clearRx() {
	ui.displayRxBrowser->clear();
	displayThread->setRxLength(0);
	rxDisplayNum(0);
	emit operationRecord(fa::eraser, tr("清除数据接收内容"));
}

