﻿#include "displayThread.h"
#include "app.h"
#include <windows.h>
#include <QtCore/QDebug>

DisplayThread::DisplayThread() {
	stoped = false;
	rxDisplay = false;
	txDisplay = false;
}

void DisplayThread::stop() {
	stoped = true;
}

void DisplayThread::run() {
	while (!stoped) {
		QString rxData;
		QString txData;
		App::textMutex.lock();
		if (rxDisplay) {
			while (App::rxStringList.count() > 0) {
				rxData += App::rxStringList.takeFirst();
			}
			rxDisplayLength += rxData.length() / 3;
		}
		else {
			App::rxStringList.clear();
		}
		if (txDisplay) {
			while (App::txStringList.count() > 0) {
				txData += App::txStringList.takeFirst();
			}
			txDisplayLength += txData.length() / 3;
		}
		else {
			App::txStringList.clear();
		}
		App::textMutex.unlock();
		if (rxData.length() > 0 && rxDisplay) {
			emit displayRxText(rxData);
			emit rxDisplayNum(rxDisplayLength);
		}
		if (txData.length() > 0 && txDisplay) {
			emit displayTxText(txData);
			emit txDisplayNum(txDisplayLength);
		}
		//阻塞显示，让刷新频率在50Hz左右
		msleep(20);
	}
	stoped = false;
}
