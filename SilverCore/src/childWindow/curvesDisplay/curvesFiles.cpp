#include <curvesFiles.h>

CurvesFiles::CurvesFiles(QVector<GraphData> *graphData) :
	_graphData(graphData) {
	//加上这一句才会自动释放对象
	this->setAttribute(Qt::WA_DeleteOnClose);
	ui.setupUi(this);
	//初始化流式布局
	_flowLayout = new QFlowLayout(ui.saveSourceBox);
	_flowLayout->setObjectName(QString::fromUtf8("flowLayout"));
	_flowLayout->setContentsMargins(9, 9, 9, 9);
	//加载checkBox
	for (int i = 0; i < _graphData->count(); i++) {
		if ((*_graphData)[i].isShow) {
			QString curvesName = (*_graphData)[i].name;
			QCheckBox *checkBox = new QCheckBox(curvesName, ui.saveSourceBox);

			SaveRange *saveange = new SaveRange(&(*_graphData)[i]);
			//添加到对应map
			_curveMap[checkBox] = saveange;

			//向图层中添加控件
			_flowLayout->addWidget(checkBox);
			QObject::connect(checkBox, SIGNAL(toggled(bool)), this, SLOT(checkBoxUpdate(bool)));
			checkBox->setChecked(true);
		}
	}
}

CurvesFiles::~CurvesFiles() {
	delete _flowLayout;
}

//比较函数
bool compare(SaveRange *a, SaveRange *b) {
	return a->graphData->graphIndex < b->graphData->graphIndex;
}

//曲线勾选 --> 选择范围的comboBox的选项更新 slots
void CurvesFiles::checkBoxUpdate(bool check) {
	QCheckBox *checkBox = (QCheckBox *)sender();
	//必须确保曲线存在
	if (_curveMap.contains(checkBox)) {
		if (!check) {
			_saveRangeList.removeOne(_curveMap[checkBox]);
		}
		else {
			_saveRangeList.append(_curveMap[checkBox]);
		}
	}
	else {
		qDebug() << "选项框对应曲线不存在";
	}
	//将保存列表按照曲线序号进行排序
	qSort(_saveRangeList.begin(), _saveRangeList.end(), compare);
	//先清空comboBox再重新添加一遍
	ui.setCurveComboBox->clear();
	for (int i = 0; i < _saveRangeList.count(); i++) {
		ui.setCurveComboBox->addItem(_saveRangeList[i]->graphData->name);
	}

	if (0 == ui.setCurveComboBox->count()) {
		ui.lowRangeIntegerSpin->setEnabled(false);
		ui.highRangeIntegerSpin->setEnabled(false);
	}
	else {
		ui.lowRangeIntegerSpin->setEnabled(true);
		ui.highRangeIntegerSpin->setEnabled(true);
		//初始化当前显示项
		getSelectedCurveRange(ui.setCurveComboBox->currentIndex());
	}
}

//检查曲线输入范围的数据 slots
void CurvesFiles::checkCurveSpinRange(void) {
	QSpinBox *box = (QSpinBox *)sender();
	if (ui.lowRangeIntegerSpin->objectName() == box->objectName()) {
		int value = qMin<int>(ui.lowRangeIntegerSpin->value(), _currentSaveRange->highKey);
		value = qMax<int>(value, _currentSaveRange->graphData->keyVec.first());
		//找到不大于key的首个iterator
		QVector<double>::iterator iter = qLowerBound(_currentSaveRange->graphData->keyVec.begin(), _currentSaveRange->graphData->keyVec.end(), value);
		_currentSaveRange->lowKey = *iter; 
		_currentSaveRange->lowKeyIndex = iter - _currentSaveRange->graphData->keyVec.begin();
		ui.lowRangeIntegerSpin->setValue((int)_currentSaveRange->lowKey);
	}
	else if (ui.highRangeIntegerSpin->objectName() == box->objectName()) {
		int value = qMax<int>(ui.highRangeIntegerSpin->value(), _currentSaveRange->lowKey);
		value = qMin<int>(value, _currentSaveRange->graphData->keyVec.last());
		//找到不大于key的首个iterator
		QVector<double>::iterator iter = qLowerBound(_currentSaveRange->graphData->keyVec.begin(), _currentSaveRange->graphData->keyVec.end(), value);
		_currentSaveRange->highKey = *iter;
		_currentSaveRange->highKeyIndex = iter - _currentSaveRange->graphData->keyVec.begin();
		ui.highRangeIntegerSpin->setValue((int)_currentSaveRange->highKey);
	}
}

//保存内容区间的数据读取
void CurvesFiles::getSelectedCurveRange(int selected) {
	_currentSaveRange = _saveRangeList[selected];

	ui.wholeRangeLabel->setText(QString("%1 - %2").arg(_currentSaveRange->graphData->keyVec.first()).arg(_currentSaveRange->graphData->keyVec.last()));
	ui.valueCountLabel->setText(QString("%1").arg(_currentSaveRange->dataCount));
	ui.lowRangeIntegerSpin->setValue((int)_currentSaveRange->lowKey);
	ui.highRangeIntegerSpin->setValue((int)_currentSaveRange->highKey);
}

//comboBox勾选 --> 更改选择范围的目标更新 slots
void CurvesFiles::selectedCurveChanged(int selected) {
	selected = qMax<int> (selected, 0);
	getSelectedCurveRange(selected);
}

//保存csv文件
void CurvesFiles::saveCsv(QString fileName) {
	//打开.csv文件
	QFile file(fileName);
	if (!file.open(QIODevice::WriteOnly | QIODevice::Text)) {
		//弹出警告窗口
		if (QMessageBox::Ok == QMessageBox::warning(this, "警告", "文件没有读取权限")) {
			return;
		}
	}
	QTextStream out(&file);

	//所有数据组中最大的长度
	int maxLength = 0;
	//标题
	for (int i = 0; i < _saveRangeList.count(); i++) {
		maxLength = qMax<int>(maxLength, _saveRangeList[i]->dataCount);
		QString keyName = _saveRangeList[i]->graphData->name + "_key";
		QString valueName = _saveRangeList[i]->graphData->name + "_value";
		//输入流
		out << keyName << "," << valueName << ","" ,";
	}
	//标题换行
	out << "\n";
	for (int j = 0; j < maxLength; j++) {
		for (int i = 0; i < _saveRangeList.count(); i++) {
			if ((_saveRangeList[i]->lowKeyIndex + j) < _saveRangeList[i]->highKeyIndex) {
				out << _saveRangeList[i]->graphData->keyVec[_saveRangeList[i]->lowKeyIndex + j] << "," << _saveRangeList[i]->graphData->valVec[_saveRangeList[i]->lowKeyIndex + j] << ","" ,";
			}
			else {
				out << " " << "," << " " << ","" ,";
			}
		}
		//内容间隔换行
		out << "\n";
	}
	//结束换行
	out << "\n";
	//流结束
	file.close();
}

//保存文件的按键 slots
void CurvesFiles::saveFiles() {
	//获取创建的csv文件名
	QString fileName = QFileDialog::getSaveFileName(this, tr("Excel file"), qApp->applicationDirPath(), tr("Files (*.csv)"));
	if (fileName.isEmpty()) {
		return;
	}
	QRegExp typeExp(".csv");
	if (fileName.indexOf(typeExp) > 0) {
		//保存csv文件
		saveCsv(fileName);
	}
	//确认按下
	accept();
}


