#include "curveCache.h"

CurveCache::CurveCache(uint16_t count, double nowKey) :
	_count(count),
	_nowKey(nowKey) {
	_curvesConfig = new QVector<CurveConfig>(_count);
	_keyVector = new QVector<CurveKey>();
}

CurveCache::~CurveCache() {
	delete _curvesConfig;
	delete _keyVector;
}

//设置缓存中某条曲线的名称
void CurveCache::setName(uint16_t i, QString name) {
	(*_curvesConfig)[i].name = name;
}
//获取缓存中某条曲线的名称
QString CurveCache::getName(uint16_t i) {
	return (*_curvesConfig)[i].name;
}

//设置缓存中某条曲线是否显示
void CurveCache::setShow(uint16_t i, bool show) {
	(*_curvesConfig)[i].isShow = show;
}
//获取缓存中某条曲线是否显示
bool CurveCache::isShow(uint16_t i) {
	return (*_curvesConfig)[i].isShow;
}

//更新当前时刻下所有曲线的键
void CurveCache::keyUpdate() {
	_nowKey++;
	CurveKey nowCurveKey;
	nowCurveKey.key = _nowKey;
	nowCurveKey.valueCache.clear();
	_keyVector->push_back(nowCurveKey);
}

//向当前最新的键缓存中存入曲线值
void CurveCache::addLastValue(uint16_t i, double value) {
	CurveValue curveValue;
	curveValue.order = i;
	curveValue.value = value;
	_keyVector->last().valueCache.push_back(curveValue);
}

//向某一个键缓存中存入曲线值
void CurveCache::addValue(double key, uint16_t i, double value) {
	CurveValue curveValue;
	curveValue.order = i;
	curveValue.value = value;
	(*_keyVector)[key].valueCache.push_back(curveValue);
}

//获取最后一个键中的缓存数据
CurveKey CurveCache::getLastCache() {
	return _keyVector->last();
}

//获取某一个键中的缓存数据
CurveKey CurveCache::getValue(double key) {
	return (*_keyVector)[key];
}

