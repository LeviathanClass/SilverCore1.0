﻿#include "settingTable.h"
#include <QtCore/QDebug>

//默认参数
const QString initName[] = {"gyroX", "gyroY", "gyroZ", \
					"accelX", "accelY", "accelZ", \
					"magX", "magY", "magZ", "pres", \
					"pitch", "roll", "yaw", \
					"q0", "q1", "q2", "q3", \
					"temp", "curve_18", "curve_19", "curve_20", \
					"curve_21", "curve_22", "curve_23", "curve_24", \
					"curve_25", "curve_26", "curve_27", "curve_28", \
					"curve_29", "curve_30", "curve_31" };

const QColor initColor[] = {
	QColor(0xC9CACA), QColor(0x009B4C), QColor(0x00A2E9), QColor(0xFFF000), \
	QColor(0xE62129), QColor(0xE40082), QColor(0xF08519), QColor(0xF4B3B3), \
	QColor(0x897870), QColor(0xA79CCB), QColor(0x758FC8), QColor(0x7C6FB0), \
	QColor(0x5E5872), QColor(0x008FD7), QColor(0x4B8D7F), QColor(0x5BA997), \
	QColor(0x56AAB7), QColor(0x009B4C), QColor(0xB9CCBC), QColor(0xBCC774), \
	QColor(0x5F5C50), QColor(0xB6B49E), QColor(0xEF8641), QColor(0xAC6249), \
	QColor(0xAF8283), QColor(0xCF7771), QColor(0xCF788A), QColor(0xE95A6F), \
	QColor(0xD49D9E), QColor(0xAC6484), QColor(0xCA5599), QColor(0x804E9A) };

const bool initCheck[] = { true, true, true , \
					true, true, true, \
					false, false, false, false, 
					true, true, true, \
					false, false, false, false, \
					false, false, false, false, \
					false, false, false, false, \
					false, false, false, false, \
					false, false, false };

//根据ini文件配置内容
void SettingTable::initState(QSettings *settings) {
	//检查文件对应的长度段
	QString countStr = settings->value("Curves/showCount").toString();
	int dataCount = 0;
	if (countStr != "") {
		dataCount = countStr.toUInt();
	}
	else {
		dataCount = 32;
		//写入ini配置文件
		settings->beginGroup("Curves");
		settings->setValue("showCount", dataCount);
		settings->endGroup();
	}

	for (int row = 0; row < dataCount; row++) {
		QWidget *selectWidget = this->cellWidget(row, 0);
		QCheckBox *checkBox = selectWidget->findChild<QCheckBox*>(QString("checkBox_%1").arg(row));
		QEditableLable *editableLable = selectWidget->findChild<QEditableLable*>(QString("editableLable_%1").arg(row));

		//检查文件对应的check段
		QString checkStr = settings->value(QString("Curves/check_%1").arg(row)).toString();
		bool check = false;
		if (checkStr != "") {
			if(checkStr == "true")
				check = true;
			else 
				check = false;
		}
		else {
			check = initCheck[row];
			//写入ini配置文件
			settings->beginGroup("Curves");
			settings->setValue(QString("check_%1").arg(row), check);
			settings->endGroup();
		}
		checkBox->setChecked(check);

		//检查文件对应的名称段
		QString nameStr = settings->value(QString("Curves/name_%1").arg(row)).toString();
		QString curveName;
		if (nameStr != "") {
			curveName = nameStr;
		}
		else {
			curveName = initName[row];
			//写入ini配置文件
			settings->beginGroup("Curves");
			settings->setValue(QString("name_%1").arg(row), curveName);
			settings->endGroup();
		}
		editableLable->setText(curveName);

		//检查文件对应的颜色段
		QString colorStr = settings->value(QString("Curves/color_%1").arg(row)).toString();
		QColor curveColor;
		if (colorStr != "") {
			curveColor = QColor::fromRgba(colorStr.toUInt());
		}
		else {
			curveColor = initColor[row];
			//写入ini配置文件
			settings->beginGroup("Curves");
			settings->setValue(QString("color_%1").arg(row), QVariant(curveColor.rgba()));
			settings->endGroup();
		}
		editableLable->setColor(curveColor);
	}
}

SettingTable::SettingTable(QWidget *parent) :
	itemCnt(32) {
	Q_UNUSED(*parent)

	this->setColumnCount(3);
	header << tr("名称") << tr("位数") << tr("数值");
	this->setHorizontalHeaderLabels(header);
	this->setStyleSheet("selection-background-color:lightblue;"); //选中项的颜色设置为亮蓝
	this->setMouseTracking(true);
	this->horizontalHeader()->setDefaultAlignment(Qt::AlignLeft);
	this->verticalHeader()->setVisible(false);  //隐藏垂直列表头
	this->horizontalHeader()->setFixedHeight(30);
	this->verticalHeader()->setDefaultSectionSize(5);
	this->horizontalHeader()->setDefaultSectionSize(10);
	this->horizontalHeader()->setStretchLastSection(true); //设置最后一列自适应大小
	this->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch); //所有列自适应列宽
	//this->verticalHeader()->setSectionResizeMode(QHeaderView::Stretch); //自适应行高
	//tableWidget->setShowGrid(false);  //隐藏单元格边框
	this->setFocusPolicy(Qt::NoFocus); //取消选中单元格时的虚线框
	this->setSelectionBehavior(QAbstractItemView::SelectRows); //选择整行
	this->setSelectionMode(QAbstractItemView::SingleSelection); //选择单行模式
	this->setEditTriggers(QAbstractItemView::NoEditTriggers); //禁止编辑单元格
	this->setMouseTracking(true);  //跟踪鼠标一定要先设的值

	for (int row = 0; row < itemCnt; row++) {
		this->insertRow(row); //动态插入新行	
		this->setRowHeight(row, 30);

		//checkbox及可编辑label
		editableWidget = new QWidget;
		editableWidget->setObjectName(QString("editableWidget_%1").arg(row));
		editableWidget->setMouseTracking(true);
		checkBox = new QCheckBox(editableWidget);
		checkBox->setObjectName(QString("checkBox_%1").arg(row));
		checkBox->setChecked(false);
		editableLable = new QEditableLable(editableWidget, initColor[row], QString("curve_%1").arg(row));
		editableLable->setObjectName(QString("editableLable_%1").arg(row));
		editableLable->setMouseTracking(true);
		editableLable->setMinimumWidth(50);
		editableLable->setMaximumWidth(60);
		//listMarkWidget.insert(row, markPointWidget);
		editableLayout = new QHBoxLayout(editableWidget);
		editableLayout->addWidget(checkBox);
		editableLayout->addWidget(editableLable);
		editableLayout->setContentsMargins(1, 1, 1, 1); //这里就可以设置单元格cell中的四个方向的间隔
		editableLayout->setAlignment(editableWidget, Qt::AlignCenter);
		editableWidget->setLayout(editableLayout);
		this->setCellWidget(row, 0, editableWidget);
		editableWidget->setMaximumWidth(80);

		//接收到的位数
		bitWidget = new QWidget;
		bitWidget->setObjectName(QString("bitWidget_%1").arg(row));
		bitWidget->setMouseTracking(true);
		bitLabel = new QLabel(bitWidget);
		bitLabel->setObjectName(QString("label_%1").arg(row));
		bitLabel->setText(tr("--"));
		bitLabel->setFont(QFont("Microsoft YaHei", 8, QFont::Normal));
		bitLabel->setMinimumWidth(40);
		bitLabel->setMaximumWidth(40);
		bitLayout = new QHBoxLayout(bitWidget);
		bitLayout->addWidget(bitLabel);
		bitLayout->setContentsMargins(1, 1, 1, 1); //这里就可以设置单元格cell中的四个方向的间隔
		bitLayout->setAlignment(bitWidget, Qt::AlignCenter);
		bitWidget->setLayout(bitLayout);
		this->setCellWidget(row, 1, bitWidget);
		bitWidget->setMinimumWidth(40);

		//显示的数值
		numberWidget = new QWidget;
		numberWidget->setObjectName(QString("numberWidget_%1").arg(row));
		numberWidget->setMouseTracking(true);
		numberLineEdit = new QLineEdit(numberWidget);
		numberLineEdit->setObjectName(QString("lineEdit_%1").arg(row));
		numberLineEdit->setText(tr("--"));
		numberLineEdit->setFont(QFont("Microsoft YaHei", 8, QFont::Normal));
		numberLineEdit->setStyleSheet("border-width:0;border-style:outset");
		numberLineEdit->setReadOnly(true);
		numberLineEdit->setMinimumWidth(50);
		numberLineEdit->setMaximumWidth(70);
		numberLayout = new QHBoxLayout(numberWidget);
		numberLayout->addWidget(numberLineEdit);
		numberLayout->setContentsMargins(1, 1, 1, 1); //这里就可以设置单元格cell中的四个方向的间隔
		numberLayout->setAlignment(numberWidget, Qt::AlignCenter);
		numberWidget->setLayout(numberLayout);
		this->setCellWidget(row, 2, numberWidget);
		numberWidget->setMinimumWidth(50);
	}


	//connect(this, SIGNAL(cellEntered(int, int)), this, SLOT(MouseTrackItem(int, int)));
	QHeaderView *sortHeader = this->horizontalHeader();
	sortHeader->setSortIndicator(0, Qt::DescendingOrder); //设置下图中第一列头部的小三角可见,且为降序标志
	sortHeader->setSortIndicatorShown(true);
	//connect(sortHeader, SIGNAL(sectionClicked(int)), this, SLOT(SortItems(int))); //链接信号和自定义槽
	//this->resizeColumnsToContents();//所有列根据内容自适应格宽
}

SettingTable::~SettingTable() {

}
