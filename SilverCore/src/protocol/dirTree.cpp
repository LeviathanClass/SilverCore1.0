#include "dirTree.h"

//释放树内存
void makeTreeEmpty(Tree T) {
	if (T->FirstChild != nullptr) {
		Tree t = T->FirstChild->NextSibling;
		makeTreeEmpty(T->FirstChild);
		T->FirstChild->length = 0;
		Tree tmp = nullptr;
		while (t != nullptr) {
			tmp = t;
			t = t->NextSibling;
			makeTreeEmpty(tmp);
		}
	}
	T->length = 0;
	T->index = 0;
	free(T);
	T = nullptr;
}

//删除文件或文件夹
void deleteTree(Tree T) {
	Tree tmp = nullptr;
	if (T->Parent != nullptr && T->Parent->FirstChild != T) {
		tmp = T->Parent->FirstChild;
		while (tmp->NextSibling != T) {
			tmp = tmp->NextSibling;
		}
		tmp->NextSibling = T->NextSibling;
		makeTreeEmpty(T);
		if (T->Parent->length > 0) {
			T->Parent->length--;
		}
	}
	else if (T->Parent != nullptr && T->Parent->FirstChild == T) {
		T->Parent->FirstChild = T->NextSibling;
		makeTreeEmpty(T);
		if (T->Parent->length > 0) {
			T->Parent->length--;
		}
	}
	else {
		makeTreeEmpty(T);
	}
}

//获取绝对路径
Tree getAbsoluteDir(Tree T, void(*decode)(void*)) {
	Tree tmp = T;
	while (tmp->Parent != nullptr) {
		if (decode != nullptr) {
			decode(tmp->Parent->data);
		}
		tmp = tmp->Parent;
	}
	return tmp;
}

//重新设置数据
void resetTreeData(Tree T, void* src) {
	T->data = src;
}

//新建一个文件
Tree newTree(void* src) {
	Tree T = (Tree)malloc(sizeof(struct TreeNode));
	T->data = src;
	T->length = 0;
	T->index = 0;
	T->FirstChild = T->LastSibling = T->NextSibling = T->Parent = nullptr;
	return T;
}

//添加某个文件或目录到指定目录中
Tree insertTree(Tree Des, Tree Src) {
	Src->Parent = Des;
	if (Des != nullptr) {
		//父tree的长度增加
		Des->length++;
		Src->index = Des->length - 1;
	}
	if (Des->FirstChild == nullptr) {
		Des->FirstChild = Src;
		Src->LastSibling = Des;
	}
	else {
		Tree tmp = Des->FirstChild;
		while (tmp->NextSibling != nullptr) {
			tmp = tmp->NextSibling;
		}
		tmp->NextSibling = Src;
		Src->LastSibling = tmp;
	}
	return Des;
}

//函数功能：返回节点的深度
int getTreeDepth(Tree T) {
	int count = 0;
	Tree tmp = T;
	while (tmp->Parent != nullptr) {
		count++;
		tmp = tmp->Parent;
	}
	return count;
}

//函数功能：返回节点的长度（子节点个数）
int getTreeChildrenLength(Tree T) {
	return T->length;
}

//函数功能：判断树的子成员是否为空
int isTreeChildEmpty(Tree T) {
	return !(T->length);
}

//函数功能：获取数据
void *getTreeData(Tree T) {
	return T->data;
}

//函数功能：返回节点的父节点
Tree getTreeParent(Tree T) {
	return T->Parent;
}

//函数功能：返回tree中第index个子节点
Tree atParentTree(Tree T, int index) {
	if (index > T->length) {
		return nullptr;
	}
	Tree tmp = T->FirstChild;
	for (int i = 0; i < index; i++) {
		tmp = tmp->NextSibling;
	}
	return tmp;
}

//函数功能：返回tree中第一个子节点
Tree beginChildOfTree(Tree T) {
	if (T->length == 0) {
		return nullptr;
	}
	Tree tmp = T->FirstChild;
	return tmp;
}

//函数功能：返回tree中最后一个子节点
Tree endChildOfTree(Tree T) {
	if (T->length == 0) {
		return nullptr;
	}
	Tree tmp = atParentTree(T, T->length - 1);
	return tmp;
}

//函数功能：获取指定内容在其父目录中的下标
int indexOfParentTree(Tree T, void *data, int(*equal)(const void*, const void*)) {
	Tree tmp = T->FirstChild;
	int length = T->length;
	for (int i = 0; i < length; i++) {
		//如果有输入钩子函数
		if (equal != nullptr) {
			if (equal(data, tmp->data)) {
				return tmp->index;
			}
		}
		//没输入钩子函数直接判断地址
		else {
			if (data == tmp->data) {
				return tmp->index;
			}
		}
		tmp = tmp->NextSibling;
	}
	//没有找到则返回-1
	return -1;
}

//函数功能：获取树指向下一个树，并返回树的地址
Tree nextOfTree(Tree *T) {
	Tree tmp = (*T);
	tmp = tmp->NextSibling;
	return tmp;
}

//函数功能：返回树的下一个树的地址
Tree getNextTree(Tree T) {
	return T->NextSibling;
}

//函数功能：获取树指向上一个树，并返回树的地址
Tree lastOfTree(Tree *T) {
	Tree tmp = (*T);
	tmp = tmp->LastSibling;
	return tmp;
}

//函数功能：返回树的上一个树的地址
Tree getLastTree(Tree T) {
	return T->LastSibling;
}

//函数功能：复制文件，符号复制
int copyTree(Tree File, Tree Dir) {
	int flag = 0;
	if (Dir->FirstChild != nullptr) {
		Tree tmp = Dir->FirstChild;
		while (tmp != nullptr) {
			if (tmp->data != File->data) {
				tmp = tmp->NextSibling;
			}
			else {
				flag = -1;
				break;
			}
		}
	}
	if (flag == 0) {
		Dir = insertTree(Dir, File);
	}
	return flag;
}


