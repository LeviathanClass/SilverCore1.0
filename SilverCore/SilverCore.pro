#-------------------------------------------------
#
# Project created by QtCreator 2020-06-08T09:43:08
#
#-------------------------------------------------

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

QT       += core gui websockets widgets xml serialport printsupport
TARGET = SilverCore
TEMPLATE = app
LIBS += -L"."
DEPENDPATH += .
DEFINES += quc
UI_DIR = ./

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

CONFIG += c++11

INCLUDEPATH += ./inc \
               ./QtAwesome \
               ./quc_include \
               ./inc/git2_mingw \

HEADERS += \
    QtAwesome/QtAwesome.h \
    QtAwesome/QtAwesomeAnim.h \
    inc/axistag.h \
    inc/dirTree.h \
    inc/git2_mingw/git2.h \
    inc/jsonConversion.h \
    inc/jsonDecode.h \
    inc/jsonDirContent.h \
    quc_include/buttongroup.h \
    quc_include/combobox.h \
    quc_include/navbutton.h \
    quc_include/quc.h \
    quc_include/switchbutton.h \
    quc_include/xprogressbar.h \
    inc/SilverCore.h \
    inc/broadcastDecode.h \
    inc/byteConverter.h \
    inc/calibrateWidget.h \
    inc/circularArray.h \
    inc/config.h \
    inc/configWidget.h \
    inc/connectThread.h \
    inc/crc.h \
    inc/curvesPlot.h \
    inc/dataWidget.h \
    inc/displayThread.h \
    inc/expandWidget.h \
    inc/firmwareWidget.h \
    inc/plotWidget.h \
    inc/qEditableLable.h \
    inc/qFlowLayout.h \
    inc/qcustomplot.h \
    inc/settingTable.h \

SOURCES += \
    src/childWindow/calibrate/calibrateWidget.cpp \
    src/childWindow/config/config.cpp \
    src/childWindow/config/configWidget.cpp \
    src/childWindow/config/jsonConversion.cpp \
    src/childWindow/config/jsonDecode.cpp \
    src/childWindow/config/jsonDirContent.cpp \
    src/childWindow/curvesDisplay/curvesPlot.cpp \
    src/childWindow/curvesDisplay/plotWidget.cpp \
    src/childWindow/curvesDisplay/settingTable.cpp \
    src/childWindow/dataDisplay/connectThread.cpp \
    src/childWindow/dataDisplay/dataWidget.cpp \
    src/childWindow/dataDisplay/displayThread.cpp \
    src/childWindow/expand/expandWidget.cpp \
    src/childWindow/firmware/firmwareWidget.cpp \
    src/control/axistag.cpp \
    src/control/qEditableLable.cpp \
    src/control/qFlowLayout.cpp \
    src/control/qcustomplot.cpp \
    src/mainWindow/SilverCore.cpp \
    src/mainWindow/app.cpp \
    src/mainWindow/main.cpp \
    src/protocol/broadcastDecode.cpp \
    src/protocol/byteConverter.cpp \
    src/protocol/circularArray.cpp \
    src/protocol/crc.cpp \
    src/protocol/dirTree.cpp

FORMS += \
    src/childWindow/calibrate/calibrateWidgetUi.ui \
    src/childWindow/config/configWidgetUi.ui \
    src/childWindow/curvesDisplay/plotWidgetUi.ui \
    src/childWindow/dataDisplay/dataWidgetUi.ui \
    src/childWindow/expand/expandWidgetUi.ui \
    src/childWindow/firmware/firmwareWidgetUi.ui \
    src/mainWindow/SilverCoreUi.ui

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

RESOURCES += \
    SilverCore.qrc

DISTFILES += \
    depend/mingw730_64/libQtAwesome.a \
    depend/mingw730_64/libQtAwesomed.a \
    depend/mingw730_64/libgit2.dll.a \
    depend/mingw730_64/libquc.a \
    depend/mingw730_64/libqucd.a \
    depend/mingw730_64/quc.dll \
    depend/mingw730_64/qucd.dll

win32:CONFIG(release, debug|release): LIBS += -L$$PWD/depend/mingw730_64/ -lQtAwesome
else:win32:CONFIG(debug, debug|release): LIBS += -L$$PWD/depend/mingw730_64/ -lQtAwesomed
else:unix:!macx: LIBS += -L$$PWD/depend/mingw730_64/ -lQtAwesome

INCLUDEPATH += $$PWD/depend/mingw730_64
DEPENDPATH += $$PWD/depend/mingw730_64

win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$PWD/depend/mingw730_64/libQtAwesome.a
else:win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$PWD/depend/mingw730_64/libQtAwesomed.a
else:win32:!win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$PWD/depend/mingw730_64/QtAwesome.lib
else:win32:!win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$PWD/depend/mingw730_64/QtAwesomed.lib
else:unix:!macx: PRE_TARGETDEPS += $$PWD/depend/mingw730_64/libQtAwesome.a

win32:CONFIG(release, debug|release): LIBS += -L$$PWD/depend/mingw730_64/ -lquc
else:win32:CONFIG(debug, debug|release): LIBS += -L$$PWD/depend/mingw730_64/ -lqucd
else:unix:!macx: LIBS += -L$$PWD/depend/mingw730_64/ -lquc

INCLUDEPATH += $$PWD/depend/mingw730_64
DEPENDPATH += $$PWD/depend/mingw730_64



win32:CONFIG(release, debug|release): LIBS += -L$$PWD/depend/mingw730_64/ -lgit2
else:win32:CONFIG(debug, debug|release): LIBS += -L$$PWD/depend/mingw730_64/ -lgit2d
else:unix:!macx: LIBS += -L$$PWD/depend/mingw730_64/ -lgit2

INCLUDEPATH += $$PWD/depend/mingw730_64
DEPENDPATH += $$PWD/depend/mingw730_64

win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$PWD/depend/mingw730_64/libgit2.a
else:win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$PWD/depend/mingw730_64/libgit2d.a
else:win32:!win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$PWD/depend/mingw730_64/git2.lib
else:win32:!win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$PWD/depend/mingw730_64/git2d.lib
else:unix:!macx: PRE_TARGETDEPS += $$PWD/depend/mingw730_64/libgit2.a
